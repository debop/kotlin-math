import org.jetbrains.dokka.gradle.DokkaTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

/*
 * Copyright (c) 2016. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

buildscript {
    extra["kotlinVersion"] = "1.3.31"
    val kotlinVersion = extra["kotlinVersion"] as String

    repositories {
        jcenter()
        maven { setUrl("https://plugins.gradle.org/m2/") }
        maven { setUrl("https://dl.bintray.com/kotlin/dokka/") }
    }

    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion")
        classpath("com.netflix.nebula:nebula-kotlin-plugin:$kotlinVersion")
        classpath("org.jetbrains.dokka:dokka-gradle-plugin:0.9.17")
    }
}

plugins {
    base
    java
    kotlin("jvm") version "1.3.31"
    id("org.jetbrains.dokka") version "0.9.17"

    // Gradle Home page에서 제공하는 build 정보
    // kotlin-math build 정보는 다음 링크에서 참조
    // https://scans.gradle.com/s/nw26kfa32rnx4
    //    `build-scan`
    `maven-publish`
}

apply {
    plugin("maven")
    plugin("nebula.kotlin")
    plugin("org.jetbrains.dokka")
}

group = "org.debop4k"

val kotlinVersion = extra["kotlinVersion"] as String

repositories {
    jcenter()
    mavenCentral()
    maven { setUrl("http://dl.bintray.com/kyonifer/maven") }
}

dependencies {
    implementation(kotlin("stdlib", kotlinVersion))
    implementation(kotlin("reflect", kotlinVersion))

    testImplementation(kotlin("test", kotlinVersion))


    implementation("org.slf4j:slf4j-api:1.7.25")
    implementation("org.apache.commons:commons-math3:3.6.1")
    implementation("org.eclipse.collections:eclipse-collections:9.2.0")

    // koma : https://github.com/kyonifer/koma
    implementation("koma:core:0.10")
    implementation("koma:backend-matrix-ejml:0.10")
    testImplementation("koma:tests:0.10")

    implementation("org.ejml:ejml-all:0.32")

    testImplementation("org.junit.jupiter:junit-jupiter:5.4.2")
}

//buildScan {
//    setLicenseAgreementUrl("https://gradle.com/terms-of-service")
//    setLicenseAgree("yes")
//
//    publishAlways()
//}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

val sourceSets = project.the<SourceSetContainer>()

val sourcesJar by tasks.creating(Jar::class) {
    from(sourceSets["main"].allSource)
    classifier = "sources"
}

val dokka by tasks.getting(DokkaTask::class) {
    outputFormat = "html"
    outputDirectory = "$buildDir/javadoc"
}

val dokkaJar by tasks.creating(Jar::class) {
    group = JavaBasePlugin.DOCUMENTATION_GROUP
    description = "Assembles Kotlin docs with Dokka"
    classifier = "javadoc"
    from(dokka)
}

tasks.withType<Test> {
    useJUnitPlatform()

    testLogging {
        events("FAILED")
    }

    maxParallelForks = Runtime.getRuntime().availableProcessors()
    setForkEvery(1L)
}

configure<PublishingExtension> {
    publications {
        register(project.name, MavenPublication::class) {
            from(components["java"])
            artifact(sourcesJar)
            artifact(dokkaJar)
            groupId = project.group as String
            artifactId = project.name
            version = project.version as String
        }
    }
}

//publishing {
//    publications {
//        create("default", MavenPublication::class.java) {
//            from(components["java"])
//            artifact(dokkaJar)
//            artifact(sourcesJar)
//        }
//    }
//
//    repositories {
//        maven {
//            url = uri("$buildDir/repository")
//        }
//    }
//}