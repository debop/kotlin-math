/*
 * Copyright (c) 2016. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:JvmName("CategoricalStatistics")

package org.debop4k.math

import org.eclipse.collections.impl.list.mutable.primitive.*

/**
 *  Extract the elements of the maximum frequency.
 *  @receiver sequence of type `T` object
 */
fun <T> Sequence<T>.mode(): List<T> {
  return countBy().entries
      .sortedByDescending { it.value }
      .toList()
      .let { list ->
        // 최대 빈도 수에 해당 놈들만 선택한다.
        list.takeWhile { list[0].value == it.value }.map { it.key }
      }
}

/**
 *  Extract the elements of the maximum frequency.
 *  @receiver iterable of type `T` object
 */
fun <T> Iterable<T>.mode(): List<T> {
  return countBy().entries
      .sortedByDescending { it.value }
      .toList()
      .let { list ->
        // 최대 빈도 수에 해당 놈들만 선택한다.
        list.takeWhile { list[0].value == it.value }.map { it.key }
      }
}

/** Extract the elements of the maximum frequency. */
fun <T> Array<out T>.mode(): List<T> = asIterable().mode()

/** Extract the elements of the maximum frequency. */
fun ByteArray.mode() = asIterable().mode()

/** Extract the elements of the maximum frequency. */
fun ByteArrayList.mode() = this.collect { it }.mode()

/** Extract the elements of the maximum frequency. */
fun ShortArray.mode() = asIterable().mode()

/** Extract the elements of the maximum frequency. */
fun ShortArrayList.mode() = this.collect { it }.mode()

/** Extract the elements of the maximum frequency. */
fun IntArray.mode() = asIterable().mode()

/** Extract the elements of the maximum frequency. */
fun IntArrayList.mode() = this.collect { it }.mode()

/** Extract the elements of the maximum frequency. */
fun LongArray.mode() = asIterable().mode()

/** Extract the elements of the maximum frequency. */
fun LongArrayList.mode() = this.collect { it }.mode()

/** Extract the elements of the maximum frequency. */
fun FloatArray.mode() = asIterable().mode()

/** Extract the elements of the maximum frequency. */
fun FloatArrayList.mode() = this.collect { it }.mode()

/** Extract the elements of the maximum frequency. */
fun DoubleArray.mode() = asIterable().mode()

/** Extract the elements of the maximum frequency. */
fun DoubleArrayList.mode() = this.collect { it }.mode()

/** 요소와 요소의 갯수를 표현하는 `Map` 을 빋드합니다 */
fun <T> Sequence<T>.countBy(): Map<T, Int> = groupApply({ it }, { it.count() })

/** 요소와 요소의 갯수를 표현하는 `Map` 을 빋드합니다 */
fun <T> Iterable<T>.countBy(): Map<T, Int> = groupApply({ it }, { it.count() })

/**
 * 요소와 요소의 갯수를 표현하는 `Map` 을 빋드합니다
 * @param keySelector key selector
 */
inline fun <T, K> Sequence<T>.countBy(keySelector: (T) -> K): Map<K, Int> =
    groupApply(keySelector) { it.count() }

/**
 * 요소와 요소의 갯수를 표현하는 `Map` 을 빋드합니다
 * @param keySelector key selector
 */
inline fun <T, K> Iterable<T>.countBy(keySelector: (T) -> K): Map<K, Int> =
    groupApply(keySelector) { it.count() }
