/*
 * Copyright (c) 2016. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:JvmName("Clustering")

package org.debop4k.math.ml

import org.apache.commons.math3.ml.clustering.*

/**
 * Clustering 을 수행할 입력 데이터
 *
 * @property item Item
 * @property location n-dimentional point
 */
data class ClusterInput<out T>(val item: T, val location: DoubleArray) : Clusterable {
    /**
     * Gets the n-dimensional point.
     */
    override fun getPoint(): DoubleArray = location

    override fun equals(other: Any?): Boolean = when (other) {
        is ClusterInput<*> -> hashCode() == other.hashCode()
        else               -> false
    }

    override fun hashCode(): Int {
        return item!!.hashCode()
    }
}

/**
 * Clustering 될 아이템의 2차원 공간의 Location
 *
 * @property x x axis value
 * @property y y axis value
 */
data class DoublePoint(val x: Double, val y: Double)

/**
 * Clustering 된 군집의 중심점과 군집 요소를 나타냅니다
 *
 * @property center Cluster 의 중심 위치
 * @property points 해당 군집에 해당하는 요소 컬렉션
 */
data class Centroid<T>(val center: DoublePoint, val points: List<T>) {
    /**
     * 지정한 요소가 해당 군집에 속했는지 판단합니다.
     */
    operator fun contains(elem: T): Boolean = points.contains(elem)
}

/**
 * 2차원 정보를 이용하여 kMeans Clustering 을 수행합니다.
 */
@JvmOverloads
fun Collection<Pair<Double, Double>>.kMeansCluster(k: Int, maxIterations: Int = 100): List<Centroid<Pair<Double, Double>>> =
        kMeansCluster(k, maxIterations, { it.first }, { it.second })

/**
 * 2차원 정보를 이용하여 kMeans Clustering 을 수행합니다.
 */
@JvmOverloads
fun Sequence<Pair<Double, Double>>.kMeansCluster(k: Int, maxIterations: Int = 100): List<Centroid<Pair<Double, Double>>> =
        kMeansCluster(k, maxIterations, { it.first }, { it.second })

/**
 * 지정한 정보로부터 2차원 정보를 추출하여 kMeans Clustering 을 수행합니다.
 */
inline fun <T> Iterable<T>.kMeansCluster(k: Int,
                                         maxIterations: Int,
                                         xSelector: (T) -> Double,
                                         ySelector: (T) -> Double): List<Centroid<T>> {
    val inputs = this.map { ClusterInput(it, doubleArrayOf(xSelector(it), ySelector(it))) }

    return KMeansPlusPlusClusterer<ClusterInput<T>>(k, maxIterations)
            .cluster(inputs)
            .map { cluster ->
                Centroid(center = (cluster.center).point.let { DoublePoint(it[0], it[1]) },
                         points = cluster.points.map { it.item })
            }
}

/**
 * 지정한 정보로부터 2차원 정보를 추출하여 kMeans Clustering 을 수행합니다.
 */
inline fun <T> Sequence<T>.kMeansCluster(k: Int,
                                         maxIterations: Int,
                                         crossinline xSelector: (T) -> Double,
                                         crossinline ySelector: (T) -> Double): List<Centroid<T>> {
    return asIterable().kMeansCluster(k, maxIterations, xSelector, ySelector)
}

/**
 * 2차원 정보를 이용하여 Fuzzy kMeans Clustering 을 수행합니다.
 */
fun Collection<Pair<Double, Double>>.fuzzyKMeansCluster(k: Int, fuzziness: Double): List<Centroid<Pair<Double, Double>>> =
        fuzzyKMeansCluster(k, fuzziness, { it.first }, { it.second })

/**
 * 2차원 정보를 이용하여 Fuzzy kMeans Clustering 을 수행합니다.
 */
fun Sequence<Pair<Double, Double>>.fuzzyKMeansCluster(k: Int, fuzziness: Double): List<Centroid<Pair<Double, Double>>> =
        fuzzyKMeansCluster(k, fuzziness, { it.first }, { it.second })

/**
 * 지정한 정보로부터 2차원 정보를 추출하여 Fuzzy kMeans Clustering 을 수행합니다.
 */
inline fun <T> Iterable<T>.fuzzyKMeansCluster(k: Int,
                                              fuzziness: Double,
                                              xSelector: (T) -> Double,
                                              ySelector: (T) -> Double): List<Centroid<T>> {
    val inputs = map { ClusterInput(it, doubleArrayOf(xSelector(it), ySelector(it))) }

    return FuzzyKMeansClusterer<ClusterInput<T>>(k, fuzziness).cluster(inputs)
            .map { cluster ->
                Centroid(center = cluster.center.point.let { DoublePoint(it[0], it[1]) },
                         points = cluster.points.map { it.item })
            }
}

/**
 * 지정한 정보로부터 2차원 정보를 추출하여 Fuzzy kMeans Clustering 을 수행합니다.
 */
inline fun <T> Sequence<T>.fuzzyKMeansCluster(k: Int,
                                              fuzziness: Double,
                                              crossinline xSelector: (T) -> Double,
                                              crossinline ySelector: (T) -> Double): List<Centroid<T>> {
    return asIterable().fuzzyKMeansCluster(k, fuzziness, xSelector, ySelector)
}


/**
 * 2차원 정보를 이용하여 Multi kMeans Clustering 을 수행합니다.
 */
fun Collection<Pair<Double, Double>>.multiKMeansCluster(k: Int,
                                                        maxIterations: Int,
                                                        trialCount: Int): List<Centroid<Pair<Double, Double>>> =
        multiKMeansCluster(k, maxIterations, trialCount, { it.first }, { it.second })

/**
 * 2차원 정보를 이용하여 Multi kMeans Clustering 을 수행합니다.
 */
fun Sequence<Pair<Double, Double>>.multiKMeansCluster(k: Int,
                                                      maxIterations: Int,
                                                      trialCount: Int): List<Centroid<Pair<Double, Double>>> =
        multiKMeansCluster(k, maxIterations, trialCount, { it.first }, { it.second })


/**
 * 지정한 정보로부터 2차원 정보를 추출하여 Multi kMeans Clustering 을 수행합니다.
 */
inline fun <T> Iterable<T>.multiKMeansCluster(k: Int,
                                              maxIterations: Int,
                                              trialCount: Int,
                                              xSelector: (T) -> Double,
                                              ySelector: (T) -> Double): List<Centroid<T>> {
    val inputs = map { ClusterInput(it, doubleArrayOf(xSelector(it), ySelector(it))) }

    return KMeansPlusPlusClusterer<ClusterInput<T>>(k, maxIterations)
            .let { cluster ->
                MultiKMeansPlusPlusClusterer<ClusterInput<T>>(cluster, trialCount)
                        .cluster(inputs)
                        .map {
                            Centroid(DoublePoint(-1.0, -1.0), it.points.map { it.item })
                        }
            }
}

/**
 * 지정한 정보로부터 2차원 정보를 추출하여 Multi kMeans Clustering 을 수행합니다.
 */
inline fun <T> Sequence<T>.multiKMeansCluster(k: Int,
                                              maxIterations: Int,
                                              trialCount: Int,
                                              crossinline xSelector: (T) -> Double,
                                              crossinline ySelector: (T) -> Double): List<Centroid<T>> {
    return asIterable().multiKMeansCluster(k, maxIterations, trialCount, xSelector, ySelector)
}


/**
 * 2차원 정보를 이용하여 db scan kMeans Clustering 을 수행합니다.
 */
fun Collection<Pair<Double, Double>>.dbScanCluster(maximumRadius: Double, minPoints: Int): List<Centroid<Pair<Double, Double>>> =
        dbScanCluster(maximumRadius, minPoints, { it.first }, { it.second })

/**
 * 2차원 정보를 이용하여 db scan kMeans Clustering 을 수행합니다.
 */
fun Sequence<Pair<Double, Double>>.dbScanCluster(maximumRadius: Double, minPoints: Int): List<Centroid<Pair<Double, Double>>> =
        dbScanCluster(maximumRadius, minPoints, { it.first }, { it.second })


/**
 * 지정한 정보로부터 2차원 정보를 추출하여 db scan kMeans Clustering 을 수행합니다.
 */
inline fun <T> Iterable<T>.dbScanCluster(maximumRadius: Double,
                                         minPoints: Int,
                                         xSelector: (T) -> Double,
                                         ySelector: (T) -> Double): List<Centroid<T>> {

    val inputs = map { ClusterInput(it, doubleArrayOf(xSelector(it), ySelector(it))) }

    return DBSCANClusterer<ClusterInput<T>>(maximumRadius, minPoints)
            .cluster(inputs)
            .map {
                Centroid(center = DoublePoint(-1.0, -1.0), points = it.points.map { it.item })
            }
}

/**
 * 지정한 정보로부터 2차원 정보를 추출하여 db scan kMeans Clustering 을 수행합니다.
 */
inline fun <T> Sequence<T>.dbScanCluster(maximumRadius: Double,
                                         minPoints: Int,
                                         crossinline xSelector: (T) -> Double,
                                         crossinline ySelector: (T) -> Double): List<Centroid<T>> {
    return asIterable().dbScanCluster(maximumRadius, minPoints, xSelector, ySelector)
}