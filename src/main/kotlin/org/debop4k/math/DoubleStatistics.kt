/*
 * Copyright (c) 2016. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:JvmName("DoubleStatistics")

package org.debop4k.math

import org.apache.commons.math3.stat.StatUtils
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.eclipse.collections.api.RichIterable
import org.eclipse.collections.impl.list.mutable.primitive.DoubleArrayList
import java.math.BigDecimal
import java.util.concurrent.atomic.AtomicBoolean

fun Double.abs() = if (this < 0.0) -this else this

val Iterable<Double>.descriptiveStatistics: Descriptives
    get() = DescriptiveStatistics().apply { forEach { addValue(it) } }.run { ApacheDescriptives(this) }

val Sequence<Double>.descriptiveStatistics: Descriptives
    get() = DescriptiveStatistics().apply { forEach { addValue(it) } }.run { ApacheDescriptives(this) }

val Array<out Double>.descriptiveStatistics: Descriptives
    get() = DescriptiveStatistics().apply { forEach { addValue(it) } }.run { ApacheDescriptives(this) }

val DoubleArray.descriptiveStatistics: Descriptives
    get() = DescriptiveStatistics().apply { forEach { addValue(it) } }.run { ApacheDescriptives(this) }

val DoubleArrayList.descriptiveStatistics: Descriptives
    get() = DescriptiveStatistics().apply { forEach { addValue(it) } }.run { ApacheDescriptives(this) }


fun Collection<Double>.geometricMean() = StatUtils.geometricMean(toDoubleArray())
fun Iterable<Double>.geometricMean() = StatUtils.geometricMean(toList().toDoubleArray())
fun Sequence<Double>.geometricMean() = StatUtils.geometricMean(toList().toDoubleArray())
fun Array<out Double>.geometricMean() = StatUtils.geometricMean(toDoubleArray())
fun DoubleArray.geometricMean() = StatUtils.geometricMean(this)
fun DoubleArrayList.geometricMean() = StatUtils.geometricMean(this.toArray())

fun Collection<Double>.percentile(percentile: Double) = StatUtils.percentile(toDoubleArray(), percentile)
fun Iterable<Double>.percentile(percentile: Double) = StatUtils.percentile(toList().toDoubleArray(), percentile)
fun Sequence<Double>.percentile(percentile: Double) = StatUtils.percentile(toList().toDoubleArray(), percentile)
fun Array<out Double>.percentile(percentile: Double) = StatUtils.percentile(toDoubleArray(), percentile)
fun DoubleArray.percentile(percentile: Double) = StatUtils.percentile(this, percentile)
fun DoubleArrayList.percentile(percentile: Double) = StatUtils.percentile(this.toArray(), percentile)

fun Collection<Double>.median() = percentile(50.0)
fun Iterable<Double>.median() = percentile(50.0)
fun Sequence<Double>.median() = percentile(50.0)
fun Array<out Double>.median() = percentile(50.0)
fun DoubleArray.median() = percentile(50.0)
// fun DoubleArrayList.median() = percentile(50.0)

fun Collection<Double>.variance() = StatUtils.variance(toDoubleArray())
fun Iterable<Double>.variance() = StatUtils.variance(toList().toDoubleArray())
fun Sequence<Double>.variance() = StatUtils.variance(toList().toDoubleArray())
fun Array<out Double>.variance() = StatUtils.variance(toDoubleArray())
fun DoubleArray.variance() = StatUtils.variance(this)
fun DoubleArrayList.variance() = StatUtils.variance(toArray())

fun Collection<Double>.sumOfSqures() = StatUtils.sumSq(toDoubleArray())
fun Iterable<Double>.sumOfSqures() = StatUtils.sumSq(toList().toDoubleArray())
fun Sequence<Double>.sumOfSqures() = StatUtils.sumSq(toList().toDoubleArray())
fun Array<out Double>.sumOfSqures() = StatUtils.sumSq(toDoubleArray())
fun DoubleArray.sumOfSqures() = StatUtils.sumSq(this)
fun DoubleArrayList.sumOfSqures() = StatUtils.sumSq(toArray())

val Iterable<Double>.standardDeviation get() = descriptiveStatistics.standardDeviation
val Sequence<Double>.standardDeviation get() = descriptiveStatistics.standardDeviation
val Array<out Double>.standardDeviation get() = descriptiveStatistics.standardDeviation
val DoubleArray.standardDeviation get() = descriptiveStatistics.standardDeviation
val DoubleArrayList.standardDeviation get() = descriptiveStatistics.standardDeviation

fun Collection<Double>.normalize(): DoubleArray = StatUtils.normalize(toDoubleArray())
fun Iterable<Double>.normalize(): DoubleArray = StatUtils.normalize(toList().toDoubleArray())
fun Sequence<Double>.normalize(): DoubleArray = StatUtils.normalize(toList().toDoubleArray())
fun Array<out Double>.normalize(): DoubleArray = StatUtils.normalize(toDoubleArray())
fun DoubleArray.normalize(): DoubleArray = StatUtils.normalize(this)
fun DoubleArrayList.normalize(): DoubleArray = StatUtils.normalize(toArray())

val Iterable<Double>.kurtosis get() = descriptiveStatistics.kurtosis
val Sequence<Double>.kurtosis get() = descriptiveStatistics.kurtosis
val Array<out Double>.kurtosis get() = descriptiveStatistics.kurtosis
val DoubleArray.kurtosis get() = descriptiveStatistics.kurtosis
val DoubleArrayList.kurtosis get() = descriptiveStatistics.kurtosis

val Iterable<Double>.skewness get() = descriptiveStatistics.skewness
val Sequence<Double>.skewness get() = descriptiveStatistics.skewness
val Array<out Double>.skewness get() = descriptiveStatistics.skewness
val DoubleArray.skewness get() = descriptiveStatistics.skewness
val DoubleArrayList.skewness get() = descriptiveStatistics.skewness


// SimpleRegression Type alias
typealias ASR = org.apache.commons.math3.stat.regression.SimpleRegression

fun Sequence<Pair<Double, Double>>.simpleRegression() = simpleRegression({ it.first }, { it.second })
fun Iterable<Pair<Double, Double>>.simpleRegression() = simpleRegression({ it.first }, { it.second })

inline fun <T> Iterable<T>.simpleRegression(xSelector: (T) -> Double,
                                            ySelector: (T) -> Double): SimpleRegression =
    asSequence().simpleRegression(xSelector, ySelector)

inline fun <T> Sequence<T>.simpleRegression(xSelector: (T) -> Double,
                                            ySelector: (T) -> Double): SimpleRegression {
  val sr = ASR()
  forEach { sr.addData(xSelector(it), ySelector(it)) }
  return ApacheSimpleRegression(sr)
}


// Aggregation Operaators

inline fun <T, K> Sequence<T>.descriptiveStaticsBy(keySelector: (T) -> K,
                                                   doubleMapper: (T) -> Double) =
    groupApply(keySelector, doubleMapper) { it.descriptiveStatistics }

inline fun <T, K> Iterable<T>.descriptiveStaticsBy(keySelector: (T) -> K,
                                                   doubleMapper: (T) -> Double) =
    groupApply(keySelector, doubleMapper) { it.descriptiveStatistics }


inline fun <T, K> Sequence<T>.sumBy(keySelector: (T) -> K, doubleMapper: (T) -> Double) =
    groupApply(keySelector, doubleMapper) { it.sum() }

inline fun <T, K> Iterable<T>.sumBy(keySelector: (T) -> K, doubleMapper: (T) -> Double) =
    groupApply(keySelector, doubleMapper) { it.sum() }

inline fun <T, K> Sequence<T>.averageBy(keySelector: (T) -> K, doubleMapper: (T) -> Double) =
    groupApply(keySelector, doubleMapper) { it.average() }

inline fun <T, K> Iterable<T>.averageBy(keySelector: (T) -> K, doubleMapper: (T) -> Double) =
    groupApply(keySelector, doubleMapper) { it.average() }

inline fun <T, K> Sequence<T>.minBy(keySelector: (T) -> K, doubleMapper: (T) -> Double) =
    groupApply(keySelector, doubleMapper) { it.min() }

inline fun <T, K> Iterable<T>.minBy(keySelector: (T) -> K, doubleMapper: (T) -> Double) =
    groupApply(keySelector, doubleMapper) { it.min() }

inline fun <T, K> Sequence<T>.maxBy(keySelector: (T) -> K, doubleMapper: (T) -> Double) =
    groupApply(keySelector, doubleMapper) { it.max() }

inline fun <T, K> Iterable<T>.maxBy(keySelector: (T) -> K, doubleMapper: (T) -> Double) =
    groupApply(keySelector, doubleMapper) { it.max() }

inline fun <T, K> Sequence<T>.medianBy(keySelector: (T) -> K, doubleMapper: (T) -> Double) =
    groupApply(keySelector, doubleMapper) { it.median() }

inline fun <T, K> Iterable<T>.medianBy(keySelector: (T) -> K, doubleMapper: (T) -> Double) =
    groupApply(keySelector, doubleMapper) { it.median() }

inline fun <T, K> Sequence<T>.percentileBy(percentile: Double, keySelector: (T) -> K, doubleMapper: (T) -> Double) =
    groupApply(keySelector, doubleMapper) { it.percentile(percentile) }

inline fun <T, K> Iterable<T>.percentileBy(percentile: Double, keySelector: (T) -> K, doubleMapper: (T) -> Double) =
    groupApply(keySelector, doubleMapper) { it.percentile(percentile) }

inline fun <T, K> Sequence<T>.varianceBy(keySelector: (T) -> K, doubleMapper: (T) -> Double) =
    groupApply(keySelector, doubleMapper) { it.variance() }

inline fun <T, K> Iterable<T>.varianceBy(keySelector: (T) -> K, doubleMapper: (T) -> Double) =
    groupApply(keySelector, doubleMapper) { it.variance() }


inline fun <T, K> Sequence<T>.stDevBy(keySelector: (T) -> K, doubleMapper: (T) -> Double) =
    groupApply(keySelector, doubleMapper) { it.standardDeviation }

inline fun <T, K> Iterable<T>.stDevBy(keySelector: (T) -> K, doubleMapper: (T) -> Double) =
    groupApply(keySelector, doubleMapper) { it.standardDeviation }

inline fun <T, K> Sequence<T>.geometricMeahBy(keySelector: (T) -> K, doubleMapper: (T) -> Double) =
    groupApply(keySelector, doubleMapper) { it.geometricMean() }

inline fun <T, K> Iterable<T>.geometricMeahBy(keySelector: (T) -> K, doubleMapper: (T) -> Double) =
    groupApply(keySelector, doubleMapper) { it.geometricMean() }


// Bin Operators

inline fun <T> Iterable<T>.binByDouble(binSize: Double,
                                       gapSize: Double,
                                       crossinline valueMapper: (T) -> Double,
                                       rangeStart: Double? = null): BinModel<List<T>, Double> =
    toList().binByDouble(binSize, gapSize, valueMapper, { it }, rangeStart)

inline fun <T, G> Iterable<T>.binByDouble(binSize: Double,
                                          gapSize: Double,
                                          crossinline valueMapper: (T) -> Double,
                                          crossinline groupOp: (List<T>) -> G,
                                          rangeStart: Double? = null): BinModel<G, Double> =
    toList().binByDouble(binSize, gapSize, valueMapper, groupOp, rangeStart)

inline fun <T> Sequence<T>.binByDouble(binSize: Double,
                                       gapSize: Double,
                                       crossinline valueMapper: (T) -> Double,
                                       rangeStart: Double? = null): BinModel<List<T>, Double> =
    toList().binByDouble(binSize, gapSize, valueMapper, { it }, rangeStart)

inline fun <T, G> Sequence<T>.binByDouble(binSize: Double,
                                          gapSize: Double,
                                          crossinline valueMapper: (T) -> Double,
                                          crossinline groupOp: (List<T>) -> G,
                                          rangeStart: Double? = null): BinModel<G, Double> =
    toList().binByDouble(binSize, gapSize, valueMapper, groupOp, rangeStart)


inline fun <T> Collection<T>.binByDouble(binSize: Double,
                                         gapSize: Double,
                                         crossinline valueMapper: (T) -> Double,
                                         rangeStart: Double? = null): BinModel<List<T>, Double> =
    binByDouble(binSize, gapSize, valueMapper, { it }, rangeStart)


inline fun <T, G> Collection<T>.binByDouble(binSize: Double,
                                            gapSize: Double,
                                            crossinline valueMapper: (T) -> Double,
                                            crossinline groupOp: (List<T>) -> G,
                                            rangeStart: Double? = null): BinModel<G, Double> {

  val groupedByC: Map<BigDecimal, List<T>> = groupBy { valueMapper(it).toBigDecimal() }
  val minC: BigDecimal = rangeStart?.toBigDecimal() ?: groupedByC.keys.min()!!
  val maxC: BigDecimal = groupedByC.keys.max()!!

  val bins = mutableListOf<ClosedRange<Double>>().apply {
    var currentRangeStart = minC
    var currentRangeEnd = minC
    val isFirst = AtomicBoolean(true)
    val binSizeBigDecimal = binSize.toBigDecimal()
    val gapSizeBigDecimal = gapSize.toBigDecimal()

    while (currentRangeEnd < maxC) {
      currentRangeEnd = currentRangeStart + binSizeBigDecimal - if (isFirst.getAndSet(false)) BigDecimal.ZERO else gapSizeBigDecimal
      add(currentRangeStart.toDouble() .. currentRangeEnd.toDouble())
      currentRangeStart = currentRangeEnd + gapSizeBigDecimal
    }
  }

  return bins //.asSequence()
      .map { it to mutableListOf<T>() }
      .map { binWithList ->
        groupedByC.entries
            .filter { it.key.toDouble() in binWithList.first }
            .forEach { binWithList.second.addAll(it.value) }
        binWithList
      }
      .map { Bin(it.first, groupOp(it.second)) }
      .toList()
      .let(::BinModel)
}

inline fun <T, G> RichIterable<T>.binByDouble(binSize: Double,
                                              gapSize: Double,
                                              crossinline valueMapper: (T) -> Double,
                                              crossinline groupOp: (List<T>) -> G,
                                              rangeStart: Double? = null): BinModel<G, Double> {

  val groupedByC = groupBy { valueMapper(it).toBigDecimal() }
  val minC: BigDecimal = rangeStart?.toBigDecimal() ?: groupedByC.keysView().min()!!
  val maxC: BigDecimal = groupedByC.keysView().max()!!

  val bins = mutableListOf<ClosedRange<Double>>().apply {
    var currentRangeStart = minC
    var currentRangeEnd = minC
    val isFirst = java.util.concurrent.atomic.AtomicBoolean(true)
    val binSizeBigDecimal = binSize.toBigDecimal()
    val gapSizeBigDecimal = gapSize.toBigDecimal()

    while (currentRangeEnd < maxC) {
      currentRangeEnd = currentRangeStart + binSizeBigDecimal - if (isFirst.getAndSet(false)) BigDecimal.ZERO else gapSizeBigDecimal
      add(currentRangeStart.toDouble() .. currentRangeEnd.toDouble())
      currentRangeStart = currentRangeEnd + gapSizeBigDecimal
    }
  }

  return bins //.asSequence()
      .map { it to mutableListOf<T>() }
      .map { binWithList ->
        groupedByC.keyMultiValuePairsView()
            .filter { it.one.toDouble() in binWithList.first }
            .forEach { binWithList.second.addAll(it.two) }
        binWithList
      }
      .map { org.debop4k.math.Bin(it.first, groupOp(it.second)) }
      .toList()
      .let(::BinModel)
}


