/*
 * Copyright (c) 2016. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:JvmName("FloatStatistics")

package org.debop4k.math

import org.apache.commons.math3.stat.StatUtils
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.eclipse.collections.api.RichIterable
import org.eclipse.collections.impl.list.mutable.primitive.FloatArrayList
import java.math.BigDecimal
import java.util.concurrent.atomic.AtomicBoolean

fun Iterable<Float>.toDoubleArray() = map { it.toDouble() }.toDoubleArray()
fun Sequence<Float>.toDoubleArray() = asIterable().toDoubleArray()
fun Array<out Float>.toDoubleArray() = asIterable().toDoubleArray()
fun FloatArray.toDoubleArray() = map { it.toDouble() }.toDoubleArray()
fun FloatArrayList.toDoubleArray() = toArray().toDoubleArray()

fun Float.abs() = if (this < 0.0) -this else this

val Iterable<Float>.descriptiveStatistics: Descriptives
    get() = DescriptiveStatistics().apply { forEach { addValue(it.toDouble()) } }.run { ApacheDescriptives(this) }

val Sequence<Float>.descriptiveStatistics: Descriptives
    get() = DescriptiveStatistics().apply { forEach { addValue(it.toDouble()) } }.run { ApacheDescriptives(this) }

val Array<out Float>.descriptiveStatistics: Descriptives
    get() = DescriptiveStatistics().apply { forEach { addValue(it.toDouble()) } }.run { ApacheDescriptives(this) }

val FloatArray.descriptiveStatistics: Descriptives
    get() = DescriptiveStatistics().apply { forEach { addValue(it.toDouble()) } }.run { ApacheDescriptives(this) }

val FloatArrayList.descriptiveStatistics: Descriptives
    get() = DescriptiveStatistics().apply { forEach { addValue(it.toDouble()) } }.run { ApacheDescriptives(this) }

fun Iterable<Float>.geometricMean() = StatUtils.geometricMean(toDoubleArray())
fun Sequence<Float>.geometricMean() = StatUtils.geometricMean(toDoubleArray())
fun Array<out Float>.geometricMean() = StatUtils.geometricMean(toDoubleArray())
fun FloatArray.geometricMean() = StatUtils.geometricMean(toDoubleArray())
fun FloatArrayList.geometricMean() = StatUtils.geometricMean(toDoubleArray())

fun Iterable<Float>.percentile(percentile: Double) = StatUtils.percentile(toDoubleArray(), percentile)
fun Sequence<Float>.percentile(percentile: Double) = StatUtils.percentile(toDoubleArray(), percentile)
fun Array<out Float>.percentile(percentile: Double) = StatUtils.percentile(toDoubleArray(), percentile)
fun FloatArray.percentile(percentile: Double) = StatUtils.percentile(toDoubleArray(), percentile)
fun FloatArrayList.percentile(percentile: Double) = StatUtils.percentile(toDoubleArray(), percentile)

fun Iterable<Float>.median() = percentile(50.0)
fun Sequence<Float>.median() = percentile(50.0)
fun Array<out Float>.median() = percentile(50.0)
fun FloatArray.median() = percentile(50.0)

fun Iterable<Float>.variance() = StatUtils.variance(toDoubleArray())
fun Sequence<Float>.variance() = StatUtils.variance(toDoubleArray())
fun Array<out Float>.variance() = StatUtils.variance(toDoubleArray())
fun FloatArray.variance() = StatUtils.variance(toDoubleArray())
fun FloatArrayList.variance() = StatUtils.variance(toDoubleArray())

fun Iterable<Float>.sumOfSqures() = StatUtils.sumSq(toDoubleArray())
fun Sequence<Float>.sumOfSqures() = StatUtils.sumSq(toDoubleArray())
fun Array<out Float>.sumOfSqures() = StatUtils.sumSq(toDoubleArray())
fun FloatArray.sumOfSqures() = StatUtils.sumSq(toDoubleArray())
fun FloatArrayList.sumOfSqures() = StatUtils.sumSq(toDoubleArray())

val Iterable<Float>.standardDeviation get() = descriptiveStatistics.standardDeviation
val Sequence<Float>.standardDeviation get() = descriptiveStatistics.standardDeviation
val Array<out Float>.standardDeviation get() = descriptiveStatistics.standardDeviation
val FloatArray.standardDeviation get() = descriptiveStatistics.standardDeviation
val FloatArrayList.standardDeviation get() = descriptiveStatistics.standardDeviation

fun Collection<Float>.normalize(): DoubleArray = StatUtils.normalize(toDoubleArray())
fun Iterable<Float>.normalize(): DoubleArray = StatUtils.normalize(toDoubleArray())
fun Sequence<Float>.normalize(): DoubleArray = StatUtils.normalize(toDoubleArray())
fun Array<out Float>.normalize(): DoubleArray = StatUtils.normalize(toDoubleArray())
fun FloatArray.normalize(): DoubleArray = StatUtils.normalize(toDoubleArray())
fun FloatArrayList.normalize(): DoubleArray = StatUtils.normalize(toDoubleArray())

val Iterable<Float>.kurtosis get() = descriptiveStatistics.kurtosis
val Sequence<Float>.kurtosis get() = descriptiveStatistics.kurtosis
val Array<out Float>.kurtosis get() = descriptiveStatistics.kurtosis
val FloatArray.kurtosis get() = descriptiveStatistics.kurtosis
val FloatArrayList.kurtosis get() = descriptiveStatistics.kurtosis

val Iterable<Float>.skewness get() = descriptiveStatistics.skewness
val Sequence<Float>.skewness get() = descriptiveStatistics.skewness
val Array<out Float>.skewness get() = descriptiveStatistics.skewness
val FloatArray.skewness get() = descriptiveStatistics.skewness
val FloatArrayList.skewness get() = descriptiveStatistics.skewness


inline fun <T, K> Sequence<T>.descriptiveStaticsBy(keySelector: (T) -> K, floatMapper: (T) -> Float) =
    groupApply(keySelector, floatMapper) { it.descriptiveStatistics }

inline fun <T, K> Iterable<T>.descriptiveStaticsBy(keySelector: (T) -> K, floatMapper: (T) -> Float) =
    groupApply(keySelector, floatMapper) { it.descriptiveStatistics }

inline fun <T, K> Sequence<T>.sumBy(keySelector: (T) -> K, floatMapper: (T) -> Float) =
    groupApply(keySelector, floatMapper) { it.sum() }

inline fun <T, K> Iterable<T>.sumBy(keySelector: (T) -> K, floatMapper: (T) -> Float) =
    groupApply(keySelector, floatMapper) { it.sum() }

inline fun <T, K> Sequence<T>.averageBy(keySelector: (T) -> K, floatMapper: (T) -> Float) =
    groupApply(keySelector, floatMapper) { it.average() }

inline fun <T, K> Iterable<T>.averageBy(keySelector: (T) -> K, floatMapper: (T) -> Float) =
    groupApply(keySelector, floatMapper) { it.average() }

inline fun <T, K> Sequence<T>.minBy(keySelector: (T) -> K, floatMapper: (T) -> Float) =
    groupApply(keySelector, floatMapper) { it.min() }

inline fun <T, K> Iterable<T>.minBy(keySelector: (T) -> K, floatMapper: (T) -> Float) =
    groupApply(keySelector, floatMapper) { it.min() }

inline fun <T, K> Sequence<T>.maxBy(keySelector: (T) -> K, floatMapper: (T) -> Float) =
    groupApply(keySelector, floatMapper) { it.max() }

inline fun <T, K> Iterable<T>.maxBy(keySelector: (T) -> K, floatMapper: (T) -> Float) =
    groupApply(keySelector, floatMapper) { it.max() }

inline fun <T, K> Sequence<T>.medianBy(keySelector: (T) -> K, floatMapper: (T) -> Float) =
    groupApply(keySelector, floatMapper) { it.median() }

inline fun <T, K> Iterable<T>.medianBy(keySelector: (T) -> K, floatMapper: (T) -> Float) =
    groupApply(keySelector, floatMapper) { it.median() }

inline fun <T, K> Sequence<T>.percentileBy(percentile: Double, keySelector: (T) -> K, floatMapper: (T) -> Float) =
    groupApply(keySelector, floatMapper) { it.percentile(percentile) }

inline fun <T, K> Iterable<T>.percentileBy(percentile: Double, keySelector: (T) -> K, floatMapper: (T) -> Float) =
    groupApply(keySelector, floatMapper) { it.percentile(percentile) }

inline fun <T, K> Sequence<T>.varianceBy(keySelector: (T) -> K, floatMapper: (T) -> Float) =
    groupApply(keySelector, floatMapper) { it.variance() }

inline fun <T, K> Iterable<T>.varianceBy(keySelector: (T) -> K, floatMapper: (T) -> Float) =
    groupApply(keySelector, floatMapper) { it.variance() }

inline fun <T, K> Sequence<T>.stDevBy(keySelector: (T) -> K, floatMapper: (T) -> Float) =
    groupApply(keySelector, floatMapper) { it.standardDeviation }

inline fun <T, K> Iterable<T>.stDevBy(keySelector: (T) -> K, floatMapper: (T) -> Float) =
    groupApply(keySelector, floatMapper) { it.standardDeviation }

inline fun <T, K> Sequence<T>.geometricMeahBy(keySelector: (T) -> K, floatMapper: (T) -> Float) =
    groupApply(keySelector, floatMapper) { it.geometricMean() }

inline fun <T, K> Iterable<T>.geometricMeahBy(keySelector: (T) -> K, floatMapper: (T) -> Float) =
    groupApply(keySelector, floatMapper) { it.geometricMean() }

inline fun <T> Iterable<T>.binByFloat(binSize: Float,
                                      gapSize: Float,
                                      crossinline valueMapper: (T) -> Float,
                                      rangeStart: Float? = null): BinModel<List<T>, Float> =
    toList().binByFloat(binSize, gapSize, valueMapper, { it }, rangeStart)

inline fun <T, G> Iterable<T>.binByFloat(binSize: Float,
                                         gapSize: Float,
                                         crossinline valueMapper: (T) -> Float,
                                         crossinline groupOp: (List<T>) -> G,
                                         rangeStart: Float? = null): BinModel<G, Float> =
    toList().binByFloat(binSize, gapSize, valueMapper, groupOp, rangeStart)

inline fun <T> Sequence<T>.binByFloat(binSize: Float,
                                      gapSize: Float,
                                      crossinline valueMapper: (T) -> Float,
                                      rangeStart: Float? = null): BinModel<List<T>, Float> =
    toList().binByFloat(binSize, gapSize, valueMapper, { it }, rangeStart)

inline fun <T, G> Sequence<T>.binByFloat(binSize: Float,
                                         gapSize: Float,
                                         crossinline valueMapper: (T) -> Float,
                                         crossinline groupOp: (List<T>) -> G,
                                         rangeStart: Float? = null): BinModel<G, Float> =
    toList().binByFloat(binSize, gapSize, valueMapper, groupOp, rangeStart)

inline fun <T> Collection<T>.binByFloat(binSize: Float,
                                        gapSize: Float,
                                        crossinline valueMapper: (T) -> Float,
                                        rangeStart: Float? = null): BinModel<List<T>, Float> =
    binByFloat(binSize, gapSize, valueMapper, { it }, rangeStart)

inline fun <T, G> Collection<T>.binByFloat(binSize: Float,
                                           gapSize: Float,
                                           crossinline valueMapper: (T) -> Float,
                                           crossinline groupOp: (List<T>) -> G,
                                           rangeStart: Float? = null): BinModel<G, Float> {

  val groupedByC: Map<BigDecimal, List<T>> = groupBy { valueMapper(it).toBigDecimal() }
  val minC: BigDecimal = rangeStart?.toBigDecimal() ?: groupedByC.keys.min()!!
  val maxC: BigDecimal = groupedByC.keys.max()!!

  val bins = mutableListOf<ClosedRange<Float>>().apply {
    var currentRangeStart = minC
    var currentRangeEnd = minC
    val isFirst = AtomicBoolean(true)
    val binSizeBigDecimal = binSize.toBigDecimal()
    val gapSizeBigDecimal = gapSize.toBigDecimal()

    while (currentRangeEnd < maxC) {
      currentRangeEnd = currentRangeStart + binSizeBigDecimal - if (isFirst.getAndSet(false)) BigDecimal.ZERO else gapSizeBigDecimal
      add(currentRangeStart.toFloat() .. currentRangeEnd.toFloat())
      currentRangeStart = currentRangeEnd + gapSizeBigDecimal
    }
  }

  return bins //.asSequence()
      .map { it to mutableListOf<T>() }
      .map { binWithList ->
        groupedByC.entries
            .filter { it.key.toFloat() in binWithList.first }
            .forEach { binWithList.second.addAll(it.value) }
        binWithList
      }
      .map { Bin(it.first, groupOp(it.second)) }
      .toList()
      .let(::BinModel)
}

inline fun <T, G> RichIterable<T>.binByFloat(binSize: Float,
                                             gapSize: Float,
                                             crossinline valueMapper: (T) -> Float,
                                             crossinline groupOp: (List<T>) -> G,
                                             rangeStart: Float? = null): BinModel<G, Float> {

  val groupedByC = groupBy { valueMapper(it).toBigDecimal() }
  val minC: BigDecimal = rangeStart?.toBigDecimal() ?: groupedByC.keysView().min()!!
  val maxC: BigDecimal = groupedByC.keysView().max()!!

  val bins = mutableListOf<ClosedRange<Float>>().apply {
    var currentRangeStart = minC
    var currentRangeEnd = minC
    val isFirst = AtomicBoolean(true)
    val binSizeBigDecimal = binSize.toBigDecimal()
    val gapSizeBigDecimal = gapSize.toBigDecimal()

    while (currentRangeEnd < maxC) {
      currentRangeEnd = currentRangeStart + binSizeBigDecimal - if (isFirst.getAndSet(false)) BigDecimal.ZERO else gapSizeBigDecimal
      add(currentRangeStart.toFloat() .. currentRangeEnd.toFloat())
      currentRangeStart = currentRangeEnd + gapSizeBigDecimal
    }
  }

  return bins //.asSequence()
      .map { it to mutableListOf<T>() }
      .map { binWithList ->
        groupedByC.keyMultiValuePairsView()
            .filter { it.one.toFloat() in binWithList.first }
            .forEach { binWithList.second.addAll(it.two) }
        binWithList
      }
      .map { Bin(it.first, groupOp(it.second)) }
      .toList()
      .let(::BinModel)
}