/*
 * Copyright (c) 2016. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:JvmName("Aggregation")

package org.debop4k.math

/**
 * [Sequence] `keySelector`의 key로 grouping 합니다
 *
 * @param keySelector Key selector for grouping
 * @param aggregator  grouping 된 values 에 대해 aggregate 를 수행합니다.
 */
inline fun <T, K, R> Sequence<T>.groupApply(keySelector: (T) -> K,
                                            aggregator: (Iterable<T>) -> R): Map<K, R> {
    return groupApply(keySelector, { it }, aggregator)
}

/**
 * [Sequence] 을 `keySelector`의 key로 grouping 합니다
 *
 * @param keySelector Key selector for grouping
 * @param valueMapper value mapper
 * @param aggregator grouping 된 values 에 대해 aggregate 를 수행합니다.
 */
inline fun <T, V, K, R> Sequence<T>.groupApply(keySelector: (T) -> K,
                                               valueMapper: (T) -> V,
                                               aggregator: (Iterable<V>) -> R): Map<K, R> {
    return this
            .groupBy(keySelector, valueMapper)
            .map { it.key to aggregator(it.value) }
            .toMap()
}


/**
 * [Iterable] 을 `keySelector`의 key로 grouping 합니다
 *
 * @param keySelector Key selector for grouping
 * @param aggregator grouping 된 values 에 대해 aggregate 를 수행합니다.
 */
inline fun <T, K, R> Iterable<T>.groupApply(keySelector: (T) -> K,
                                            aggregator: (Iterable<T>) -> R): Map<K, R> {
    return groupApply(keySelector, { it }, aggregator)
}

/**
 * [Iterable] 을 `keySelector`의 key로 grouping 합니다
 *
 * @param keySelector Key selector for grouping
 * @param valueMapper value mapper
 * @param aggregator grouping 된 values 에 대해 aggregate 를 수행합니다.
 */
inline fun <T, K, V, R> Iterable<T>.groupApply(keySelector: (T) -> K,
                                               valueMapper: (T) -> V,
                                               aggregator: (Iterable<V>) -> R): Map<K, R> {
    return this
            .groupBy(keySelector, valueMapper)
            .map { it.key to aggregator(it.value) }
            .toMap()
}