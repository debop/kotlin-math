/*
 * Copyright (c) 2016. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:JvmName("BigNumberx")

package org.debop4k.math

import java.math.BigDecimal
import java.math.BigInteger

/** [Int] to [BigInteger] */
fun Int.toBigInt(): BigInteger = BigInteger.valueOf(toLong())

/** [Long] to [BigInteger] */
fun Long.toBigInt(): BigInteger = BigInteger.valueOf(this)

/** [Float] to [BigInteger] */
fun Float.toBigInt(): BigInteger = BigInteger.valueOf(toLong())

/** [Double] to [BigInteger] */
fun Double.toBigInt(): BigInteger = BigInteger.valueOf(toLong())

/** [Int] to [BigDecimal] */
fun Int.toBigDecimal(): BigDecimal = BigDecimal.valueOf(toLong())

/** [Long] to [BigDecimal] */
fun Long.toBigDecimal(): BigDecimal = BigDecimal.valueOf(this)

/** [Float] to [BigDecimal] */
fun Float.toBigDecimal(): BigDecimal = BigDecimal.valueOf(toDouble())

/** [Double] to [BigDecimal] */
fun Double.toBigDecimal(): BigDecimal = BigDecimal.valueOf(this)

/** Convert [String] to [BigDecimal] */
fun String.toBigDecimal(): BigDecimal? = toBigDecimalOrNull()
