/*
 * Copyright (c) 2016. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:JvmName("LongStatistics")

package org.debop4k.math

import org.apache.commons.math3.stat.StatUtils
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.eclipse.collections.api.RichIterable
import org.eclipse.collections.impl.list.mutable.primitive.LongArrayList
import java.math.BigDecimal
import java.util.concurrent.atomic.AtomicBoolean

fun Iterable<Long>.toDoubleArray() = map { it.toDouble() }.toDoubleArray()
fun Sequence<Long>.toDoubleArray() = asIterable().toDoubleArray()
fun Array<out Long>.toDoubleArray() = asIterable().toDoubleArray()
fun LongArray.toDoubleArray() = map { it.toDouble() }.toDoubleArray()
fun LongArrayList.toDoubleArray() = toArray().toDoubleArray()

fun Long.abs(): Long = if (this < 0L) -this else this

val Iterable<Long>.descriptiveStatistics: Descriptives
    get() = DescriptiveStatistics().apply { forEach { addValue(it.toDouble()) } }.run { ApacheDescriptives(this) }
val Sequence<Long>.descriptiveStatistics: Descriptives
    get() = DescriptiveStatistics().apply { forEach { addValue(it.toDouble()) } }.run { ApacheDescriptives(this) }
val Array<out Long>.descriptiveStatistics: Descriptives
    get() = DescriptiveStatistics().apply { forEach { addValue(it.toDouble()) } }.run { ApacheDescriptives(this) }
val LongArray.descriptiveStatistics: Descriptives
    get() = DescriptiveStatistics().apply { forEach { addValue(it.toDouble()) } }.run { ApacheDescriptives(this) }
val LongArrayList.descriptiveStatistics: Descriptives
    get() = DescriptiveStatistics().apply { forEach { addValue(it.toDouble()) } }.run { ApacheDescriptives(this) }

fun Iterable<Long>.geometricMean() = StatUtils.geometricMean(toDoubleArray())
fun Sequence<Long>.geometricMean() = StatUtils.geometricMean(toDoubleArray())
fun Array<out Long>.geometricMean() = StatUtils.geometricMean(toDoubleArray())
fun LongArray.geometricMean() = StatUtils.geometricMean(toDoubleArray())
fun LongArrayList.geometricMean() = StatUtils.geometricMean(toDoubleArray())

fun Iterable<Long>.percentile(percentile: Double) = StatUtils.percentile(toDoubleArray(), percentile)
fun Sequence<Long>.percentile(percentile: Double) = StatUtils.percentile(toDoubleArray(), percentile)
fun Array<out Long>.percentile(percentile: Double) = StatUtils.percentile(toDoubleArray(), percentile)
fun LongArray.percentile(percentile: Double) = StatUtils.percentile(toDoubleArray(), percentile)
fun LongArrayList.percentile(percentile: Double) = StatUtils.percentile(toDoubleArray(), percentile)

fun Iterable<Long>.median() = percentile(50.0)
fun Sequence<Long>.median() = percentile(50.0)
fun Array<out Long>.median() = percentile(50.0)
fun LongArray.median() = percentile(50.0)

fun Iterable<Long>.variance() = StatUtils.variance(toDoubleArray())
fun Sequence<Long>.variance() = StatUtils.variance(toDoubleArray())
fun Array<out Long>.variance() = StatUtils.variance(toDoubleArray())
fun LongArray.variance() = StatUtils.variance(toDoubleArray())
fun LongArrayList.variance() = StatUtils.variance(toDoubleArray())

fun Iterable<Long>.sumOfSqures() = StatUtils.sumSq(toDoubleArray())
fun Sequence<Long>.sumOfSqures() = StatUtils.sumSq(toDoubleArray())
fun Array<out Long>.sumOfSqures() = StatUtils.sumSq(toDoubleArray())
fun LongArray.sumOfSqures() = StatUtils.sumSq(toDoubleArray())
fun LongArrayList.sumOfSqures() = StatUtils.sumSq(toDoubleArray())

val Iterable<Long>.standardDeviation get() = descriptiveStatistics.standardDeviation
val Sequence<Long>.standardDeviation get() = descriptiveStatistics.standardDeviation
val Array<out Long>.standardDeviation get() = descriptiveStatistics.standardDeviation
val LongArray.standardDeviation get() = descriptiveStatistics.standardDeviation
val LongArrayList.standardDeviation get() = descriptiveStatistics.standardDeviation

fun Collection<Long>.normalize(): DoubleArray = StatUtils.normalize(toDoubleArray())
fun Iterable<Long>.normalize(): DoubleArray = StatUtils.normalize(toDoubleArray())
fun Sequence<Long>.normalize(): DoubleArray = StatUtils.normalize(toDoubleArray())
fun Array<out Long>.normalize(): DoubleArray = StatUtils.normalize(toDoubleArray())
fun LongArray.normalize(): DoubleArray = StatUtils.normalize(toDoubleArray())
fun LongArrayList.normalize(): DoubleArray = StatUtils.normalize(toDoubleArray())

val Iterable<Long>.kurtosis get() = descriptiveStatistics.kurtosis
val Sequence<Long>.kurtosis get() = descriptiveStatistics.kurtosis
val Array<out Long>.kurtosis get() = descriptiveStatistics.kurtosis
val LongArray.kurtosis get() = descriptiveStatistics.kurtosis
val LongArrayList.kurtosis get() = descriptiveStatistics.kurtosis

val Iterable<Long>.skewness get() = descriptiveStatistics.skewness
val Sequence<Long>.skewness get() = descriptiveStatistics.skewness
val Array<out Long>.skewness get() = descriptiveStatistics.skewness
val LongArray.skewness get() = descriptiveStatistics.skewness
val LongArrayList.skewness get() = descriptiveStatistics.skewness

inline fun <T, K> Sequence<T>.descriptiveStaticsBy(keySelector: (T) -> K, longMapper: (T) -> Long) =
    groupApply(keySelector, longMapper) { it.descriptiveStatistics }

inline fun <T, K> Iterable<T>.descriptiveStaticsBy(keySelector: (T) -> K, longMapper: (T) -> Long) =
    groupApply(keySelector, longMapper) { it.descriptiveStatistics }

inline fun <T, K> Sequence<T>.sumBy(keySelector: (T) -> K, longMapper: (T) -> Long) =
    groupApply(keySelector, longMapper) { it.sum() }

inline fun <T, K> Iterable<T>.sumBy(keySelector: (T) -> K, longMapper: (T) -> Long) =
    groupApply(keySelector, longMapper) { it.sum() }

inline fun <T, K> Sequence<T>.averageBy(keySelector: (T) -> K, longMapper: (T) -> Long) =
    groupApply(keySelector, longMapper) { it.average() }

inline fun <T, K> Iterable<T>.averageBy(keySelector: (T) -> K, longMapper: (T) -> Long) =
    groupApply(keySelector, longMapper) { it.average() }

inline fun <T, K> Sequence<T>.minBy(keySelector: (T) -> K, longMapper: (T) -> Long) =
    groupApply(keySelector, longMapper) { it.min() }

inline fun <T, K> Iterable<T>.minBy(keySelector: (T) -> K, longMapper: (T) -> Long) =
    groupApply(keySelector, longMapper) { it.min() }

inline fun <T, K> Sequence<T>.maxBy(keySelector: (T) -> K, longMapper: (T) -> Long) =
    groupApply(keySelector, longMapper) { it.max() }

inline fun <T, K> Iterable<T>.maxBy(keySelector: (T) -> K, longMapper: (T) -> Long) =
    groupApply(keySelector, longMapper) { it.max() }

inline fun <T, K> Sequence<T>.medianBy(keySelector: (T) -> K, longMapper: (T) -> Long) =
    groupApply(keySelector, longMapper) { it.median() }

inline fun <T, K> Iterable<T>.medianBy(keySelector: (T) -> K, longMapper: (T) -> Long) =
    groupApply(keySelector, longMapper) { it.median() }

inline fun <T, K> Sequence<T>.percentileBy(percentile: Double, keySelector: (T) -> K, longMapper: (T) -> Long) =
    groupApply(keySelector, longMapper) { it.percentile(percentile) }

inline fun <T, K> Iterable<T>.percentileBy(percentile: Double, keySelector: (T) -> K, longMapper: (T) -> Long) =
    groupApply(keySelector, longMapper) { it.percentile(percentile) }

inline fun <T, K> Sequence<T>.varianceBy(keySelector: (T) -> K, longMapper: (T) -> Long) =
    groupApply(keySelector, longMapper) { it.variance() }

inline fun <T, K> Iterable<T>.varianceBy(keySelector: (T) -> K, longMapper: (T) -> Long) =
    groupApply(keySelector, longMapper) { it.variance() }

inline fun <T, K> Sequence<T>.stDevBy(keySelector: (T) -> K, longMapper: (T) -> Long) =
    groupApply(keySelector, longMapper) { it.standardDeviation }

inline fun <T, K> Iterable<T>.stDevBy(keySelector: (T) -> K, longMapper: (T) -> Long) =
    groupApply(keySelector, longMapper) { it.standardDeviation }

inline fun <T, K> Sequence<T>.geometricMeahBy(keySelector: (T) -> K, longMapper: (T) -> Long) =
    groupApply(keySelector, longMapper) { it.geometricMean() }

inline fun <T, K> Iterable<T>.geometricMeahBy(keySelector: (T) -> K, longMapper: (T) -> Long) =
    groupApply(keySelector, longMapper) { it.geometricMean() }

inline fun <T> Iterable<T>.binByLong(binSize: Long,
                                     gapSize: Long,
                                     crossinline valueMapper: (T) -> Long,
                                     rangeStart: Long? = null): BinModel<List<T>, Long> =
    toList().binByLong(binSize, gapSize, valueMapper, { it }, rangeStart)

inline fun <T, G> Iterable<T>.binByLong(binSize: Long,
                                        gapSize: Long,
                                        crossinline valueMapper: (T) -> Long,
                                        crossinline groupOp: (List<T>) -> G,
                                        rangeStart: Long? = null): BinModel<G, Long> =
    toList().binByLong(binSize, gapSize, valueMapper, groupOp, rangeStart)

inline fun <T> Sequence<T>.binByLong(binSize: Long,
                                     gapSize: Long,
                                     crossinline valueMapper: (T) -> Long,
                                     rangeStart: Long? = null): BinModel<List<T>, Long> =
    toList().binByLong(binSize, gapSize, valueMapper, { it }, rangeStart)

inline fun <T, G> Sequence<T>.binByLong(binSize: Long,
                                        gapSize: Long,
                                        crossinline valueMapper: (T) -> Long,
                                        crossinline groupOp: (List<T>) -> G,
                                        rangeStart: Long? = null): BinModel<G, Long> =
    toList().binByLong(binSize, gapSize, valueMapper, groupOp, rangeStart)

inline fun <T> Collection<T>.binByLong(binSize: Long,
                                       gapSize: Long,
                                       crossinline valueMapper: (T) -> Long,
                                       rangeStart: Long? = null): BinModel<List<T>, Long> =
    binByLong(binSize, gapSize, valueMapper, { it }, rangeStart)

inline fun <T, G> Collection<T>.binByLong(binSize: Long,
                                          gapSize: Long,
                                          crossinline valueMapper: (T) -> Long,
                                          crossinline groupOp: (List<T>) -> G,
                                          rangeStart: Long? = null): BinModel<G, Long> {

  val groupedByC: Map<BigDecimal, List<T>> = groupBy { valueMapper(it).toBigDecimal() }
  val minC: BigDecimal = rangeStart?.toBigDecimal() ?: groupedByC.keys.min()!!
  val maxC: BigDecimal = groupedByC.keys.max()!!

  val bins = mutableListOf<ClosedRange<Long>>().apply {
    var currentRangeStart = minC
    var currentRangeEnd = minC
    val isFirst = AtomicBoolean(true)
    val binSizeBigDecimal = binSize.toBigDecimal()
    val gapSizeBigDecimal = gapSize.toBigDecimal()

    while (currentRangeEnd < maxC) {
      currentRangeEnd = currentRangeStart + binSizeBigDecimal - if (isFirst.getAndSet(false)) BigDecimal.ZERO else gapSizeBigDecimal
      add(currentRangeStart.toLong() .. currentRangeEnd.toLong())
      currentRangeStart = currentRangeEnd + gapSizeBigDecimal
    }
  }

  return bins //.asSequence()
      .map { it to mutableListOf<T>() }
      .map { binWithList ->
        groupedByC.entries
            .filter { it.key.toLong() in binWithList.first }
            .forEach { binWithList.second.addAll(it.value) }
        binWithList
      }
      .map { Bin(it.first, groupOp(it.second)) }
      .toList()
      .let(::BinModel)
}

inline fun <T, G> RichIterable<T>.binByLong(binSize: Long,
                                            gapSize: Long,
                                            crossinline valueMapper: (T) -> Long,
                                            crossinline groupOp: (List<T>) -> G,
                                            rangeStart: Long? = null): BinModel<G, Long> {

  val groupedByC = groupBy { valueMapper(it).toBigDecimal() }
  val minC: BigDecimal = rangeStart?.toBigDecimal() ?: groupedByC.keysView().min()!!
  val maxC: BigDecimal = groupedByC.keysView().max()!!

  val bins = mutableListOf<ClosedRange<Long>>().apply {
    var currentRangeStart = minC
    var currentRangeEnd = minC
    val isFirst = AtomicBoolean(true)
    val binSizeBigDecimal = binSize.toBigDecimal()
    val gapSizeBigDecimal = gapSize.toBigDecimal()

    while (currentRangeEnd < maxC) {
      currentRangeEnd = currentRangeStart + binSizeBigDecimal - if (isFirst.getAndSet(false)) BigDecimal.ZERO else gapSizeBigDecimal
      add(currentRangeStart.toLong() .. currentRangeEnd.toLong())
      currentRangeStart = currentRangeEnd + gapSizeBigDecimal
    }
  }

  return bins //.asSequence()
      .map { it to mutableListOf<T>() }
      .map { binWithList ->
        groupedByC.keyMultiValuePairsView()
            .filter { it.one.toLong() in binWithList.first }
            .forEach { binWithList.second.addAll(it.two) }
        binWithList
      }
      .map { Bin(it.first, groupOp(it.second)) }
      .toList()
      .let(::BinModel)
}