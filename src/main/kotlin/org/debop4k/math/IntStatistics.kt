/*
 * Copyright (c) 2016. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
@file:JvmName("IntStatistics")


package org.debop4k.math

import org.apache.commons.math3.stat.StatUtils
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.eclipse.collections.api.RichIterable
import org.eclipse.collections.impl.list.mutable.primitive.IntArrayList
import java.math.BigDecimal
import java.util.concurrent.atomic.AtomicBoolean

fun Iterable<Int>.toDoubleArray() = map { it.toDouble() }.toDoubleArray()
fun Sequence<Int>.toDoubleArray() = asIterable().toDoubleArray()
fun Array<out Int>.toDoubleArray() = asIterable().toDoubleArray()
fun IntArray.toDoubleArray() = map { it.toDouble() }.toDoubleArray()
fun IntArrayList.toDoubleArray() = toArray().toDoubleArray()

fun Int.abs(): Int = if (this < 0) -this else this

val Iterable<Int>.descriptiveStatistics: Descriptives
    get() = DescriptiveStatistics().apply { forEach { addValue(it.toDouble()) } }.run { ApacheDescriptives(this) }
val Sequence<Int>.descriptiveStatistics: Descriptives
    get() = DescriptiveStatistics().apply { forEach { addValue(it.toDouble()) } }.run { ApacheDescriptives(this) }
val Array<out Int>.descriptiveStatistics: Descriptives
    get() = DescriptiveStatistics().apply { forEach { addValue(it.toDouble()) } }.run { ApacheDescriptives(this) }
val IntArray.descriptiveStatistics: Descriptives
    get() = DescriptiveStatistics().apply { forEach { addValue(it.toDouble()) } }.run { ApacheDescriptives(this) }
val IntArrayList.descriptiveStatistics: Descriptives
    get() = DescriptiveStatistics().apply { forEach { addValue(it.toDouble()) } }.run { ApacheDescriptives(this) }

fun Iterable<Int>.geometricMean() = StatUtils.geometricMean(toDoubleArray())
fun Sequence<Int>.geometricMean() = StatUtils.geometricMean(toDoubleArray())
fun Array<out Int>.geometricMean() = StatUtils.geometricMean(toDoubleArray())
fun IntArray.geometricMean() = StatUtils.geometricMean(toDoubleArray())
fun IntArrayList.geometricMean() = StatUtils.geometricMean(toDoubleArray())

fun Iterable<Int>.percentile(percentile: Double) = StatUtils.percentile(toDoubleArray(), percentile)
fun Sequence<Int>.percentile(percentile: Double) = StatUtils.percentile(toDoubleArray(), percentile)
fun Array<out Int>.percentile(percentile: Double) = StatUtils.percentile(toDoubleArray(), percentile)
fun IntArray.percentile(percentile: Double) = StatUtils.percentile(toDoubleArray(), percentile)
fun IntArrayList.percentile(percentile: Double) = StatUtils.percentile(toDoubleArray(), percentile)

fun Iterable<Int>.median() = percentile(50.0)
fun Sequence<Int>.median() = percentile(50.0)
fun Array<out Int>.median() = percentile(50.0)
fun IntArray.median() = percentile(50.0)

fun Iterable<Int>.variance() = StatUtils.variance(toDoubleArray())
fun Sequence<Int>.variance() = StatUtils.variance(toDoubleArray())
fun Array<out Int>.variance() = StatUtils.variance(toDoubleArray())
fun IntArray.variance() = StatUtils.variance(toDoubleArray())
fun IntArrayList.variance() = StatUtils.variance(toDoubleArray())

fun Iterable<Int>.sumOfSqures() = StatUtils.sumSq(toDoubleArray())
fun Sequence<Int>.sumOfSqures() = StatUtils.sumSq(toDoubleArray())
fun Array<out Int>.sumOfSqures() = StatUtils.sumSq(toDoubleArray())
fun IntArray.sumOfSqures() = StatUtils.sumSq(toDoubleArray())
fun IntArrayList.sumOfSqures() = StatUtils.sumSq(toDoubleArray())

val Iterable<Int>.standardDeviation get() = descriptiveStatistics.standardDeviation
val Sequence<Int>.standardDeviation get() = descriptiveStatistics.standardDeviation
val Array<out Int>.standardDeviation get() = descriptiveStatistics.standardDeviation
val IntArray.standardDeviation get() = descriptiveStatistics.standardDeviation
val IntArrayList.standardDeviation get() = descriptiveStatistics.standardDeviation

fun Collection<Int>.normalize(): DoubleArray = StatUtils.normalize(toDoubleArray())
fun Iterable<Int>.normalize(): DoubleArray = StatUtils.normalize(toDoubleArray())
fun Sequence<Int>.normalize(): DoubleArray = StatUtils.normalize(toDoubleArray())
fun Array<out Int>.normalize(): DoubleArray = StatUtils.normalize(toDoubleArray())
fun IntArray.normalize(): DoubleArray = StatUtils.normalize(toDoubleArray())
fun IntArrayList.normalize(): DoubleArray = StatUtils.normalize(toDoubleArray())

val Iterable<Int>.kurtosis get() = descriptiveStatistics.kurtosis
val Sequence<Int>.kurtosis get() = descriptiveStatistics.kurtosis
val Array<out Int>.kurtosis get() = descriptiveStatistics.kurtosis
val IntArray.kurtosis get() = descriptiveStatistics.kurtosis
val IntArrayList.kurtosis get() = descriptiveStatistics.kurtosis

val Iterable<Int>.skewness get() = descriptiveStatistics.skewness
val Sequence<Int>.skewness get() = descriptiveStatistics.skewness
val Array<out Int>.skewness get() = descriptiveStatistics.skewness
val IntArray.skewness get() = descriptiveStatistics.skewness
val IntArrayList.skewness get() = descriptiveStatistics.skewness

inline fun <T, K> Sequence<T>.descriptiveStaticsBy(keySelector: (T) -> K, intMapper: (T) -> Int) =
    groupApply(keySelector, intMapper) { it.descriptiveStatistics }

inline fun <T, K> Iterable<T>.descriptiveStaticsBy(keySelector: (T) -> K, intMapper: (T) -> Int) =
    groupApply(keySelector, intMapper) { it.descriptiveStatistics }

inline fun <T, K> Sequence<T>.sumBy(keySelector: (T) -> K, intMapper: (T) -> Int) =
    groupApply(keySelector, intMapper) { it.sum() }

inline fun <T, K> Iterable<T>.sumBy(keySelector: (T) -> K, intMapper: (T) -> Int) =
    groupApply(keySelector, intMapper) { it.sum() }

inline fun <T, K> Sequence<T>.averageBy(keySelector: (T) -> K, intMapper: (T) -> Int) =
    groupApply(keySelector, intMapper) { it.average() }

inline fun <T, K> Iterable<T>.averageBy(keySelector: (T) -> K, intMapper: (T) -> Int) =
    groupApply(keySelector, intMapper) { it.average() }

inline fun <T, K> Sequence<T>.minBy(keySelector: (T) -> K, intMapper: (T) -> Int) =
    groupApply(keySelector, intMapper) { it.min() }

inline fun <T, K> Iterable<T>.minBy(keySelector: (T) -> K, intMapper: (T) -> Int) =
    groupApply(keySelector, intMapper) { it.min() }

inline fun <T, K> Sequence<T>.maxBy(keySelector: (T) -> K, intMapper: (T) -> Int) =
    groupApply(keySelector, intMapper) { it.max() }

inline fun <T, K> Iterable<T>.maxBy(keySelector: (T) -> K, intMapper: (T) -> Int) =
    groupApply(keySelector, intMapper) { it.max() }

inline fun <T, K> Sequence<T>.medianBy(keySelector: (T) -> K, intMapper: (T) -> Int) =
    groupApply(keySelector, intMapper) { it.median() }

inline fun <T, K> Iterable<T>.medianBy(keySelector: (T) -> K, intMapper: (T) -> Int) =
    groupApply(keySelector, intMapper) { it.median() }

inline fun <T, K> Sequence<T>.percentileBy(percentile: Double, keySelector: (T) -> K, intMapper: (T) -> Int) =
    groupApply(keySelector, intMapper) { it.percentile(percentile) }

inline fun <T, K> Iterable<T>.percentileBy(percentile: Double, keySelector: (T) -> K, intMapper: (T) -> Int) =
    groupApply(keySelector, intMapper) { it.percentile(percentile) }

inline fun <T, K> Sequence<T>.varianceBy(keySelector: (T) -> K, intMapper: (T) -> Int) =
    groupApply(keySelector, intMapper) { it.variance() }

inline fun <T, K> Iterable<T>.varianceBy(keySelector: (T) -> K, intMapper: (T) -> Int) =
    groupApply(keySelector, intMapper) { it.variance() }

inline fun <T, K> Sequence<T>.stDevBy(keySelector: (T) -> K, intMapper: (T) -> Int) =
    groupApply(keySelector, intMapper) { it.standardDeviation }

inline fun <T, K> Iterable<T>.stDevBy(keySelector: (T) -> K, intMapper: (T) -> Int) =
    groupApply(keySelector, intMapper) { it.standardDeviation }

inline fun <T, K> Sequence<T>.geometricMeahBy(keySelector: (T) -> K, intMapper: (T) -> Int) =
    groupApply(keySelector, intMapper) { it.geometricMean() }

inline fun <T, K> Iterable<T>.geometricMeahBy(keySelector: (T) -> K, intMapper: (T) -> Int) =
    groupApply(keySelector, intMapper) { it.geometricMean() }

inline fun <T> Iterable<T>.binByInt(binSize: Int,
                                    gapSize: Int,
                                    crossinline valueMapper: (T) -> Int,
                                    rangeStart: Int? = null): BinModel<List<T>, Int> =
    toList().binByInt(binSize, gapSize, valueMapper, { it }, rangeStart)

inline fun <T, G> Iterable<T>.binByInt(binSize: Int,
                                       gapSize: Int,
                                       crossinline valueMapper: (T) -> Int,
                                       crossinline groupOp: (List<T>) -> G,
                                       rangeStart: Int? = null): BinModel<G, Int> =
    toList().binByInt(binSize, gapSize, valueMapper, groupOp, rangeStart)

inline fun <T> Sequence<T>.binByInt(binSize: Int,
                                    gapSize: Int,
                                    crossinline valueMapper: (T) -> Int,
                                    rangeStart: Int? = null): BinModel<List<T>, Int> =
    toList().binByInt(binSize, gapSize, valueMapper, { it }, rangeStart)

inline fun <T, G> Sequence<T>.binByInt(binSize: Int,
                                       gapSize: Int,
                                       crossinline valueMapper: (T) -> Int,
                                       crossinline groupOp: (List<T>) -> G,
                                       rangeStart: Int? = null): BinModel<G, Int> =
    toList().binByInt(binSize, gapSize, valueMapper, groupOp, rangeStart)

inline fun <T> Collection<T>.binByInt(binSize: Int,
                                      gapSize: Int,
                                      crossinline valueMapper: (T) -> Int,
                                      rangeStart: Int? = null): BinModel<List<T>, Int> =
    binByInt(binSize, gapSize, valueMapper, { it }, rangeStart)

inline fun <T, G> Collection<T>.binByInt(binSize: Int,
                                         gapSize: Int,
                                         crossinline valueMapper: (T) -> Int,
                                         crossinline groupOp: (List<T>) -> G,
                                         rangeStart: Int? = null): BinModel<G, Int> {

  val groupedByC: Map<BigDecimal, List<T>> = groupBy { valueMapper(it).toBigDecimal() }
  val minC: BigDecimal = rangeStart?.toBigDecimal() ?: groupedByC.keys.min()!!
  val maxC: BigDecimal = groupedByC.keys.max()!!

  val bins = mutableListOf<ClosedRange<Int>>().apply {
    var currentRangeStart = minC
    var currentRangeEnd = minC
    val isFirst = AtomicBoolean(true)
    val binSizeBigDecimal = binSize.toBigDecimal()
    val gapSizeBigDecimal = gapSize.toBigDecimal()

    while (currentRangeEnd < maxC) {
      currentRangeEnd = currentRangeStart + binSizeBigDecimal - if (isFirst.getAndSet(false)) BigDecimal.ZERO else gapSizeBigDecimal
      add(currentRangeStart.toInt() .. currentRangeEnd.toInt())
      currentRangeStart = currentRangeEnd + gapSizeBigDecimal
    }
  }

  return bins //.asSequence()
      .map { it to mutableListOf<T>() }
      .map { binWithList ->
        groupedByC.entries
            .filter { it.key.toInt() in binWithList.first }
            .forEach { binWithList.second.addAll(it.value) }
        binWithList
      }
      .map { Bin(it.first, groupOp(it.second)) }
      .toList()
      .let(::BinModel)
}

inline fun <T, G> RichIterable<T>.binByInt(binSize: Int,
                                           gapSize: Int,
                                           crossinline valueMapper: (T) -> Int,
                                           crossinline groupOp: (List<T>) -> G,
                                           rangeStart: Int? = null): BinModel<G, Int> {

  val groupedByC = groupBy { valueMapper(it).toBigDecimal() }
  val minC: BigDecimal = rangeStart?.toBigDecimal() ?: groupedByC.keysView().min()!!
  val maxC: BigDecimal = groupedByC.keysView().max()!!

  val bins = mutableListOf<ClosedRange<Int>>().apply {
    var currentRangeStart = minC
    var currentRangeEnd = minC
    val isFirst = AtomicBoolean(true)
    val binSizeBigDecimal = binSize.toBigDecimal()
    val gapSizeBigDecimal = gapSize.toBigDecimal()

    while (currentRangeEnd < maxC) {
      currentRangeEnd = currentRangeStart + binSizeBigDecimal - if (isFirst.getAndSet(false)) BigDecimal.ZERO else gapSizeBigDecimal
      add(currentRangeStart.toInt() .. currentRangeEnd.toInt())
      currentRangeStart = currentRangeEnd + gapSizeBigDecimal
    }
  }

  return bins //.asSequence()
      .map { it to mutableListOf<T>() }
      .map { binWithList ->
        groupedByC.keyMultiValuePairsView()
            .filter { it.one.toInt() in binWithList.first }
            .forEach { binWithList.second.addAll(it.two) }
        binWithList
      }
      .map { Bin(it.first, groupOp(it.second)) }
      .toList()
      .let(::BinModel)
}