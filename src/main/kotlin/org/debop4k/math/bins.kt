/*
 * Copyright (c) 2016. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:JvmName("Bins")

package org.debop4k.math

import org.eclipse.collections.impl.list.mutable.FastList
import java.util.concurrent.atomic.*

/**
 * Histogram 의 하나의 Bar 를 나타낸다
 */
class Bin<out T, in C : Comparable<C>>(val range: ClosedRange<in C>, val value: T) {

  operator fun contains(key: C) = key in range

  override fun toString(): String = "Bin(range=$range, value=$value)"
}

/**
 * Histogram 정보를 나타낸다
 */
class BinModel<out T, in C : Comparable<C>>(val bins: List<Bin<T, C>>) : Iterable<Bin<T, C>> by bins {
  operator fun get(key: C): Bin<T, C>? = bins.find { key in it.range }
  operator fun contains(key: C) = bins.any { key in it.range }
  override fun toString(): String = "BinModel(bins=$bins)"
}

/**
 * Histogram 을 만든다
 */
inline fun <T, C : Comparable<C>> Iterable<T>.binByComparable(binIncrements: Int,
                                                              incrementer: (C) -> C,
                                                              valueMapper: (T) -> C,
                                                              rangeStart: C? = null): BinModel<List<T>, C> {
  return binByComparable(binIncrements, incrementer, valueMapper, { it }, rangeStart)
}

/**
 * Histogram 을 만든다
 */
inline fun <T, C : Comparable<C>, G> Iterable<T>.binByComparable(binIncrements: Int,
                                                                 incrementer: (C) -> C,
                                                                 valueMapper: (T) -> C,
                                                                 groupOp: (List<T>) -> G,
                                                                 rangeStart: C? = null): BinModel<G, C> {
  val groupByC: Map<C, List<T>> = groupBy(valueMapper)
  val minC: C = rangeStart ?: groupByC.keys.min()!!
  val maxC: C = groupByC.keys.max()!!

  val bins = FastList<ClosedRange<C>>().apply {
    var currentRangeStart = minC
    var currentRangeEnd = minC

    val isFirst = AtomicBoolean(true)
    while (currentRangeEnd < maxC) {
      val increments = if (isFirst.getAndSet(false)) binIncrements - 1 else binIncrements
      repeat(increments) {
        currentRangeEnd = incrementer(currentRangeEnd)
      }
      add(currentRangeStart .. currentRangeEnd)
      currentRangeStart = incrementer(currentRangeEnd)
    }
  }

  return bins
      .map { it to mutableListOf<T>() }
      .map { binWithList ->
        groupByC.entries.asSequence()
            .filter { it.key in binWithList.first }
            .forEach { binWithList.second.addAll(it.value) }
        binWithList
      }
      .map { Bin(it.first, groupOp(it.second)) }
      .toList()
      .let(::BinModel)
}

