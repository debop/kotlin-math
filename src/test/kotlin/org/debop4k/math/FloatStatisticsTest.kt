/*
 * Copyright (c) 2016. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.math

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.time.Month.MAY

class FloatStatisticsTest {

    @Test
    fun testMedian() {
        assertEquals(3.0, sequenceOf(1.0f, 3.0f, 5.0f).median())
    }

    @Test
    fun testMedian2() {
        assertEquals(3.5, sequenceOf(1.0f, 3.0f, 4.0f, 5.0f).median())
    }

    @Test
    fun testVariance() {
        assertEquals(1.0, floatArrayOf(2.0f, 3.0f, 4.0f).variance())
        assertEquals(14.25, floatArrayOf(1.0f, 4.0f, 6.0f, 10.0f).variance())
    }

    data class Item(val name: String, val value: Float)

    @Test
    fun `test operators`() {

        val list = listOf(Item("Alpha", 4.0f),
                          Item("Beta", 6.0f),
                          Item("Gamma", 7.2f),
                          Item("Delta", 9.2f),
                          Item("Epsilon", 6.8f),
                          Item("Zeta", 2.4f),
                          Item("Iota", 8.8f))

        // find sums by name length
        val sumsByLengths = list.sumBy(keySelector = { it.name.length }, floatMapper = { it.value })
        println("Sums by lengths: $sumsByLengths")
        // assertThat(sumsByLengths).isEqualTo(mapOf(4 to 17.2, 5 to 20.4, 7 to 6.8))

        // find average by name length
        val averagesByLengths = list.averageBy(keySelector = { it.name.length }, floatMapper = { it.value })
        println("Averages by length: $averagesByLengths")

        // find standard deviations by name length
        val stdevsByLengths = list.stDevBy(keySelector = { it.name.length }, floatMapper = { it.value })
        println("Standard Deviations by length: $stdevsByLengths")
    }

    data class Product(val id: Int,
                       val name: String,
                       val category: String,
                       val section: Int,
                       val defectRate: Float)

    data class CategoryAndSection(val category: String, val section: Int)

    @Test
    fun `multiple keys`() {
        val products = listOf(Product(1, "Rayzeon", "ABR", 3, 1.1f),
                              Product(2, "ZenFire", "ABZ", 4, 0.7f),
                              Product(3, "HydroFlux", "ABR", 3, 1.9f),
                              Product(4, "IceFlyer", "ZBN", 1, 2.4f),
                              Product(5, "FireCoyote", "ABZ", 4, 3.2f),
                              Product(6, "LightFiber", "ABZ", 2, 5.1f),
                              Product(7, "PyroKit", "ABR", 3, 1.4f),
                              Product(8, "BladeKit", "ZBN", 1, 0.5f),
                              Product(9, "NightHawk", "ZBN", 1, 3.5f),
                              Product(10, "NoctoSquirrel", "ABR", 2, 1.1f),
                              Product(11, "WolverinePack", "ABR", 3, 1.2f))

        val countByCategoryAndSection = products.countBy { CategoryAndSection(it.category, it.section) }
        println("Count by Category and Section")
        countByCategoryAndSection.forEach { println("\t$it") }


        val averageDefectByCategoryAndSection =
            products.averageBy(keySelector = { CategoryAndSection(it.category, it.section) },
                               floatMapper = { it.defectRate })

        println("Average defect rate by Category and Section")
        averageDefectByCategoryAndSection.forEach { println("\t$it") }
    }


    data class Sale(val accountId: Int, val date: LocalDate, val value: Float)

    @Test
    fun `slicing by ranges and bins by quarter`() {
        val sales = listOf(Sale(1, LocalDate.of(2016, 12, 3), 180.0f),
                           Sale(2, LocalDate.of(2016, 7, 4), 140.2f),
                           Sale(3, LocalDate.of(2016, 6, 3), 111.4f),
                           Sale(4, LocalDate.of(2016, 1, 5), 192.7f),
                           Sale(5, LocalDate.of(2016, 5, 4), 137.9f),
                           Sale(6, LocalDate.of(2016, 3, 6), 125.6f),
                           Sale(7, LocalDate.of(2016, 12, 4), 164.3f),
                           Sale(8, LocalDate.of(2016, 7, 11), 144.2f))

        // Histogram 을 만든다
        val byQuarter = sales.binByComparable(binIncrements = 3,
                                              incrementer = { it.plus(1L) },
                                              valueMapper = { it.date.month })
        byQuarter.forEach(::println)

        assertTrue {
            byQuarter[MAY]!!.value.containsAll(listOf(Sale(3, LocalDate.of(2016, 6, 3), 111.4f),
                                                      Sale(5, LocalDate.of(2016, 5, 4), 137.9f)))
        }
    }

    @Test fun `slicing by ranges and bins by quarter and sum`() {
        val sales = listOf(Sale(1, LocalDate.of(2016, 12, 3), 180.0f),
                           Sale(2, LocalDate.of(2016, 7, 4), 140.2f),
                           Sale(3, LocalDate.of(2016, 6, 3), 111.4f),
                           Sale(4, LocalDate.of(2016, 1, 5), 192.7f),
                           Sale(5, LocalDate.of(2016, 5, 4), 137.9f),
                           Sale(6, LocalDate.of(2016, 3, 6), 125.6f),
                           Sale(7, LocalDate.of(2016, 12, 4), 164.3f),
                           Sale(8, LocalDate.of(2016, 7, 11), 144.2f))

        // Histogram 을 만든다
        val byQuarter = sales.binByComparable(binIncrements = 3,
                                              incrementer = { it.plus(1L) },
                                              valueMapper = { it.date.month },
                                              groupOp = { it.map { sale -> sale.value }.sum() })
        byQuarter.forEach(::println)

        assertEquals(111.4 + 137.9, byQuarter[MAY]!!.value.toDouble(), 1.0e-4)
    }

    @Test fun `bin by float ranges`() {
        val sales = listOf(Sale(1, LocalDate.of(2016, 12, 3), 180.0f),
                           Sale(2, LocalDate.of(2016, 7, 4), 140.2f),
                           Sale(3, LocalDate.of(2016, 6, 3), 111.4f),
                           Sale(4, LocalDate.of(2016, 1, 5), 192.7f),
                           Sale(5, LocalDate.of(2016, 5, 4), 137.9f),
                           Sale(6, LocalDate.of(2016, 3, 6), 125.6f),
                           Sale(7, LocalDate.of(2016, 12, 4), 164.3f),
                           Sale(8, LocalDate.of(2016, 7, 11), 144.2f))

        // Histogram 을 만든다
        val binned: BinModel<List<Sale>, Float> = sales.binByFloat(binSize = 20.0f,
                                                                   gapSize = 0.01f,
                                                                   valueMapper = { it.value },
                                                                   rangeStart = 100.0f)
        binned.forEach(::println)

        assertTrue { binned[110.0f]!!.value.contains(Sale(3, LocalDate.of(2016, 6, 3), 111.4f)) }
    }

}