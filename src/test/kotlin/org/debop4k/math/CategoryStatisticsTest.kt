package org.debop4k.math

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class CategoryStatisticsTest {

    @Test
    fun `mode for int`() {
        // 2의 빈도수 3, 나머지는 1
        assertEquals(setOf(2), listOf(2, 54, 67, 3, 4, 5, 2, 2).mode().toSet())
    }

    @Test
    fun `mode for int array`() {
        // 2의 빈도수 3, 4의 빈도수 3, 나머지는 1, 2
        assertEquals(setOf(2, 4), arrayOf(2, 4, 54, 4, 67, 3, 4, 5, 3, 2, 2).mode().toSet())
    }

    @Test
    fun `groupApply for String`() {
        listOf("Alpha", "Beta", "Gamma", "Delta", "Epsilon")
            .groupApply(keySelector = { it.length },
                        aggregator = { str -> str.flatMap { it.split("") }.filter { it.isNotEmpty() }.count() })
            .let {
                assertEquals(mapOf(5 to 15, 4 to 4, 7 to 7), it)
            }
    }
}