/*
 * Copyright (c) 2016. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.math

import org.debop4k.math.ml.Gender.FEMALE
import org.debop4k.math.ml.Gender.MALE
import org.debop4k.math.ml.Patient
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.time.Month.MAY

class DoubleStatisticsTest {

    @Test
    fun testMedian() {
        assertEquals(3.0, sequenceOf(1.0, 3.0, 5.0).median())
    }

    @Test
    fun testMedian2() {
        assertEquals(3.5, sequenceOf(1.0, 3.0, 4.0, 5.0).median())
    }

    @Test
    fun testVariance() {
        assertEquals(1.0, doubleArrayOf(2.0, 3.0, 4.0).variance())
        assertEquals(14.25, doubleArrayOf(1.0, 4.0, 6.0, 10.0).variance())
    }

    data class Item(val name: String, val value: Double)

    @Test
    fun `test operators`() {

        val list = listOf(Item("Alpha", 4.0),
                          Item("Beta", 6.0),
                          Item("Gamma", 7.2),
                          Item("Delta", 9.2),
                          Item("Epsilon", 6.8),
                          Item("Zeta", 2.4),
                          Item("Iota", 8.8))

        // find sums by name length
        val sumsByLengths = list.sumBy(keySelector = { it.name.length }, doubleMapper = { it.value })
        println("Sums by lengths: $sumsByLengths")
        // assertThat(sumsByLengths).isEqualTo(mapOf(4 to 17.2, 5 to 20.4, 7 to 6.8))

        // find average by name length
        val averagesByLengths = list.averageBy(keySelector = { it.name.length }, doubleMapper = { it.value })
        println("Averages by length: $averagesByLengths")

        // find standard deviations by name length
        val stdevsByLengths = list.stDevBy(keySelector = { it.name.length }, doubleMapper = { it.value })
        println("Standard Deviations by length: $stdevsByLengths")
    }

    data class Product(val id: Int,
                       val name: String,
                       val category: String,
                       val section: Int,
                       val defectRate: Double)

    data class CategoryAndSection(val category: String, val section: Int)

    @Test
    fun `multiple keys`() {
        val products = listOf(Product(1, "Rayzeon", "ABR", 3, 1.1),
                              Product(2, "ZenFire", "ABZ", 4, 0.7),
                              Product(3, "HydroFlux", "ABR", 3, 1.9),
                              Product(4, "IceFlyer", "ZBN", 1, 2.4),
                              Product(5, "FireCoyote", "ABZ", 4, 3.2),
                              Product(6, "LightFiber", "ABZ", 2, 5.1),
                              Product(7, "PyroKit", "ABR", 3, 1.4),
                              Product(8, "BladeKit", "ZBN", 1, 0.5),
                              Product(9, "NightHawk", "ZBN", 1, 3.5),
                              Product(10, "NoctoSquirrel", "ABR", 2, 1.1),
                              Product(11, "WolverinePack", "ABR", 3, 1.2))

        val countByCategoryAndSection = products.countBy { CategoryAndSection(it.category, it.section) }
        println("Count by Category and Section")
        countByCategoryAndSection.forEach { println("\t$it") }


        val averageDefectByCategoryAndSection =
            products.averageBy(keySelector = { CategoryAndSection(it.category, it.section) },
                               doubleMapper = { it.defectRate })

        println("Average defect rate by Category and Section")
        averageDefectByCategoryAndSection.forEach { println("\t$it") }
    }

    @Test
    fun `simple regression`() {
        val r = listOf(1.0 to 3.00,
                       2.0 to 5.99,
                       3.0 to 9.00,
                       4.0 to 12.01).simpleRegression()

        assertEquals(3.004, r.slope)
        println("regression slope=${r.slope}, mse=${r.meanSquareError}")

    }

    data class SaleDate(val date: LocalDate, val sales: Int)

    @Test
    fun `class data regression`() {

        val salesDates = listOf(SaleDate(LocalDate.of(2017, 1, 1), 1080),
                                SaleDate(LocalDate.of(2017, 1, 2), 2010),
                                SaleDate(LocalDate.of(2017, 1, 3), 1020),
                                SaleDate(LocalDate.of(2017, 1, 4), 907),
                                SaleDate(LocalDate.of(2017, 1, 5), 805),
                                SaleDate(LocalDate.of(2017, 1, 6), 2809),
                                SaleDate(LocalDate.of(2017, 1, 7), 2600))

        val regression = salesDates.simpleRegression(xSelector = { it.date.dayOfYear.toDouble() },
                                                     ySelector = { it.sales.toDouble() })

        println("slope=${regression.slope}")
    }

    data class Sale(val accountId: Int, val date: LocalDate, val value: Double)

    @Test
    fun `slicing by ranges and bins by quarter`() {
        val sales = listOf(Sale(1, LocalDate.of(2016, 12, 3), 180.0),
                           Sale(2, LocalDate.of(2016, 7, 4), 140.2),
                           Sale(3, LocalDate.of(2016, 6, 3), 111.4),
                           Sale(4, LocalDate.of(2016, 1, 5), 192.7),
                           Sale(5, LocalDate.of(2016, 5, 4), 137.9),
                           Sale(6, LocalDate.of(2016, 3, 6), 125.6),
                           Sale(7, LocalDate.of(2016, 12, 4), 164.3),
                           Sale(8, LocalDate.of(2016, 7, 11), 144.2))

        // Histogram 을 만든다
        val byQuarter = sales.binByComparable(binIncrements = 3,
                                              incrementer = { it.plus(1L) },
                                              valueMapper = { it.date.month })
        byQuarter.forEach(::println)

        //    assertThat(byQuarter[MAY]!!.value).contains(Sale(3, LocalDate.of(2016, 6, 3), 111.4),
        //                                                Sale(5, LocalDate.of(2016, 5, 4), 137.9))
        assertTrue {
            byQuarter[MAY]!!.value.containsAll(listOf(
                Sale(3, LocalDate.of(2016, 6, 3), 111.4),
                Sale(5, LocalDate.of(2016, 5, 4), 137.9)))
        }
    }

    @Test fun `slicing by ranges and bins by quarter and sum`() {
        val sales = listOf(Sale(1, LocalDate.of(2016, 12, 3), 180.0),
                           Sale(2, LocalDate.of(2016, 7, 4), 140.2),
                           Sale(3, LocalDate.of(2016, 6, 3), 111.4),
                           Sale(4, LocalDate.of(2016, 1, 5), 192.7),
                           Sale(5, LocalDate.of(2016, 5, 4), 137.9),
                           Sale(6, LocalDate.of(2016, 3, 6), 125.6),
                           Sale(7, LocalDate.of(2016, 12, 4), 164.3),
                           Sale(8, LocalDate.of(2016, 7, 11), 144.2))

        // Histogram 을 만든다
        val byQuarter = sales.binByComparable(binIncrements = 3,
                                              incrementer = { it.plus(1L) },
                                              valueMapper = { it.date.month },
                                              groupOp = { it.map { x -> x.value }.sum() })
        byQuarter.forEach(::println)

        assertEquals(111.4 + 137.9, byQuarter[MAY]!!.value)
    }

    @Test fun `bin by double ranges`() {
        val sales = listOf(Sale(1, LocalDate.of(2016, 12, 3), 180.0),
                           Sale(2, LocalDate.of(2016, 7, 4), 140.2),
                           Sale(3, LocalDate.of(2016, 6, 3), 111.4),
                           Sale(4, LocalDate.of(2016, 1, 5), 192.7),
                           Sale(5, LocalDate.of(2016, 5, 4), 137.9),
                           Sale(6, LocalDate.of(2016, 3, 6), 125.6),
                           Sale(7, LocalDate.of(2016, 12, 4), 164.3),
                           Sale(8, LocalDate.of(2016, 7, 11), 144.2))

        // Histogram 을 만든다
        val binned = sales.binByDouble(binSize = 20.0,
                                       gapSize = 0.01,
                                       valueMapper = { it.value },
                                       rangeStart = 100.0)
        binned.forEach(::println)

        assertTrue { binned[110.0]!!.value.contains(Sale(3, LocalDate.of(2016, 6, 3), 111.4)) }
    }

    @Test fun `percentile by extension methods`() {
        val patients = listOf(Patient("John", "Simone", MALE, LocalDate.of(1989, 1, 7), 4500),
                              Patient("Sarah", "Marley", FEMALE, LocalDate.of(1970, 2, 5), 6700),
                              Patient("Jessica", "Arnold", FEMALE, LocalDate.of(1980, 3, 9), 3400),
                              Patient("Sam", "Beasley", MALE, LocalDate.of(1981, 4, 17), 8800),
                              Patient("Dan", "Forney", MALE, LocalDate.of(1985, 9, 13), 5400),
                              Patient("Lauren", "Michaels", FEMALE, LocalDate.of(1975, 8, 21), 5000),
                              Patient("Michael", "Erlich", MALE, LocalDate.of(1985, 12, 17), 4100),
                              Patient("Jason", "Miles", MALE, LocalDate.of(1991, 11, 1), 3900),
                              Patient("Rebekah", "Earley", FEMALE, LocalDate.of(1985, 2, 18), 4600),
                              Patient("James", "Larson", MALE, LocalDate.of(1974, 4, 10), 5100),
                              Patient("Dan", "Ulrech", MALE, LocalDate.of(1991, 7, 11), 6000),
                              Patient("Heather", "Eisner", FEMALE, LocalDate.of(1994, 3, 6), 6000),
                              Patient("Jasper", "Martin", MALE, LocalDate.of(1971, 7, 1), 6000))

        fun Collection<Patient>.wbccPercentileByGender(percentile: Double) =
            percentileBy(percentile = percentile,
                         keySelector = { it.gender },
                         doubleMapper = { it.whiteBloodCellCount.toDouble() })

        val percentiles = listOf(1.0, 25.0, 50.0, 75.0, 95.0, 99.0, 100.0)

        val percentileByGender = percentiles.map { it to patients.wbccPercentileByGender(it) }

        percentileByGender.forEach(::println)

    }


}