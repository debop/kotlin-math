/*
 * Copyright (c) 2016. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.math.stats

import org.apache.commons.math3.stat.ranking.NaNStrategy
import org.apache.commons.math3.stat.ranking.NaturalRanking
import org.apache.commons.math3.stat.ranking.TiesStrategy
import org.junit.jupiter.api.Test

/**
 * org.debop4k.math.stats.RankTest
 * @author debop
 * @since 17. 8. 22
 */
class RankTest {

    private fun DoubleArray.ranking(): Map<Double, Double> {
        val ranks = NaturalRanking(NaNStrategy.MINIMAL, TiesStrategy.MAXIMUM).rank(this)
        return this.mapIndexed { index, value -> value to ranks.size - ranks[index] }.toMap()
    }

    @Test
    fun `double ranking`() {

        val scores = doubleArrayOf(20.0, 17.0, 30.0, 42.3, 17.0, 50.0,
                                   Double.NaN, Double.NEGATIVE_INFINITY, 17.0, Double.POSITIVE_INFINITY)
        val scoreAndRanks = scores.ranking()
        scoreAndRanks.forEach {
            println("score=${it.key}, rank=${it.value}")
        }
    }
}