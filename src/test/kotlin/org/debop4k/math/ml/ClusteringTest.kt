/*
 * Copyright (c) 2016. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.math.ml

import org.junit.jupiter.api.Test
import java.time.LocalDate

class ClusteringTest {

    val patients = listOf(
        Patient("John", "Simone", Gender.MALE, LocalDate.of(1989, 1, 7), 4500),
        Patient("Sarah", "Marley", Gender.FEMALE, LocalDate.of(1970, 2, 5), 6700),
        Patient("Jessica", "Arnold", Gender.FEMALE, LocalDate.of(1980, 3, 9), 3400),
        Patient("Sam", "Beasley", Gender.MALE, LocalDate.of(1981, 4, 17), 8800),
        Patient("Dan", "Forney", Gender.MALE, LocalDate.of(1985, 9, 13), 5400),
        Patient("Lauren", "Michaels", Gender.FEMALE, LocalDate.of(1975, 8, 21), 5000),
        Patient("Michael", "Erlich", Gender.MALE, LocalDate.of(1985, 12, 17), 4100),
        Patient("Jason", "Miles", Gender.MALE, LocalDate.of(1991, 11, 1), 3900),
        Patient("Rebekah", "Earley", Gender.FEMALE, LocalDate.of(1985, 2, 18), 4600),
        Patient("James", "Larson", Gender.MALE, LocalDate.of(1974, 4, 10), 5100),
        Patient("Dan", "Ulrech", Gender.MALE, LocalDate.of(1991, 7, 11), 6000),
        Patient("Heather", "Eisner", Gender.FEMALE, LocalDate.of(1994, 3, 6), 6000),
        Patient("Jasper", "Martin", Gender.MALE, LocalDate.of(1971, 7, 1), 6000))

    @Test
    fun `kMeans cluster`() {
        val clusters = patients.kMeansCluster(k = 3,
                                              maxIterations = 10000,
                                              xSelector = { it.age.toDouble() },
                                              ySelector = { it.whiteBloodCellCount.toDouble() })

        clusters.forEachIndexed { index, item ->
            println("CENTROID: $index")
            item.points.forEach(::println)
        }
    }

    @Test
    fun `fuzzy kMeans cluster`() {
        val clusters = patients.fuzzyKMeansCluster(k = 3,
                                                   fuzziness = 10.0,
                                                   xSelector = { it.age.toDouble() },
                                                   ySelector = { it.whiteBloodCellCount.toDouble() })

        clusters.forEachIndexed { index, item ->
            println("CENTROID: $index")
            item.points.forEach(::println)
        }
    }

    @Test
    fun `Multi kMeans cluster`() {
        // 나이와 백혈구 수로 clustering
        val clusters: List<Centroid<Patient>> =
            patients.multiKMeansCluster(k = 3,
                                        maxIterations = 10000,
                                        trialCount = 50,
                                        xSelector = { it.age.toDouble() },
                                        ySelector = { it.whiteBloodCellCount.toDouble() })

        // print out the clusters
        clusters.forEachIndexed { index, item ->
            println("CENTROID: $index")
            item.points.forEach(::println)
        }
    }

    @Test
    fun `DBSCAN cluster`() {
        val clusters = patients.dbScanCluster(maximumRadius = 250.0,
                                              minPoints = 1,
                                              xSelector = { it.age.toDouble() },
                                              ySelector = { it.whiteBloodCellCount.toDouble() })

        clusters.forEachIndexed { index, item ->
            println("CENTROID: $index")
            item.points.forEach(::println)
        }
    }
}