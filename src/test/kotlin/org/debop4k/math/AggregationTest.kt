/*
 * Copyright (c) 2016. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.math

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class AggregationTest {

    class Email(val subject: String, val sender: String)

    data class FieldDistributions(val subject: Map<String, Int>, val sender: Map<String, Int>)

    val data = listOf(Email("I make u offer", "princeofnigeria2000@aol.com"),
                      Email("Congratulations!", "lotterybacklog@gmail.com"),
                      Email("Your Inheritance is waiting!", "lotterybacklog@gmail.com"),
                      Email("Hey", "jessica47@gmail.com"))

    @Test
    fun `countBy aggregation`() {
        val distributions = data.let {
            FieldDistributions(subject = it.countBy { it.subject },
                               sender = it.countBy { it.sender })
        }
        println(distributions)

        //    Assertions.assertThat(distributions.sender)
        //        .hasSize(3)
        //        .containsEntry("jessica47@gmail.com", 1)
        //        .containsEntry("lotterybacklog@gmail.com", 2)

        assertEquals(3, distributions.sender.size)
        assertTrue { distributions.sender["jessica47@gmail.com"] == 1 }
        assertTrue { distributions.sender["lotterybacklog@gmail.com"] == 2 }
    }
}